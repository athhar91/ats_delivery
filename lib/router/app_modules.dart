import 'package:flutter_delivery_app/modules/dashboad_page/index.dart';
import 'package:flutter_delivery_app/modules/map_view/mapview/mapview_binding.dart';
import 'package:flutter_delivery_app/modules/my_order_details_page/controllers/my_orders_detail_binding.dart';
import 'package:flutter_delivery_app/modules/my_order_details_page/view/my_orders__detail_page.dart';
import 'package:flutter_delivery_app/modules/my_orders_page.dart/view/my_orders_page.dart';
import 'package:flutter_delivery_app/modules/my_orders_page.dart/view/qr_scanner.dart';
import 'package:flutter_delivery_app/modules/notification_page/index.dart';
import 'package:flutter_delivery_app/modules/profile_page/controllers/profile_binding.dart';
import 'package:get/get.dart';

import '../modules/change_password_page/controllers/change_password_binding.dart';
import '../modules/change_password_page/index.dart';
import '../modules/dashboad_page/controllers/dashboard_binding.dart';
import '../modules/map_view/mapview/mapview_page.dart';
import '../modules/my_orders_page.dart/controllers/my_orders_binding.dart';
import '../modules/my_orders_page.dart/controllers/qr_binding.dart';
import '../modules/notification_page/controllers/notification_binding.dart';
import '../modules/otp_page/controllers/otp_binding.dart';
import '../modules/otp_page/index.dart';
import '../modules/profile_page/index.dart';
import '../modules/reset_password_page/controllers/reset_password_binding.dart';
import '../modules/reset_password_page/index.dart';
import '../modules/signin_page/controllers/signin_binding.dart';
import '../modules/signin_page/index.dart';

import '../modules/splash_page/app_controllers/app_binding.dart';
import '../modules/splash_page/index.dart';
import 'package:flutter_delivery_app/router/app_routes.dart';

class AppPages {
  static var routes = [
    GetPage(
      name: splashRoute,
      page: () => SplashPage(),
      binding: AppBinding(),
    ),
    GetPage(
      name: loginRoute,
      page: () => const SigninPage(),
      binding: SigninBinding(),
    ),
    GetPage(
      name: loginRoute,
      page: () => const SigninPage(),
      binding: SigninBinding(),
    ),
    GetPage(
      name: profileRoute,
      page: () => const ProfilePage(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: RESET_PASSWORD,
      page: () => const ResetPasswordPage(),
      binding: ResetPasswordBinding(),
    ),
    GetPage(
      name: OTP,
      page: () => const OtpPage(),
      binding: OtpBinding(),
    ),
    GetPage(
      name: change_password_route,
      page: () => const ChangePasswordPage(),
      binding: ChangePasswordBinding(),
    ),
    GetPage(
      name: dashboardRoute,
      page: () => const DashBoardPage(),
      bindings: [DashBoardBinding(), AppBinding()],
    ),
    GetPage(
      name: MY_ORDERS,
      page: () => MyOrders(),
      binding: MyOrdersBinding(),
    ),
    GetPage(
      name: MY_ORDERS_DETAIL,
      page: () => MyOrdersDetails(),
      binding: MyOrdersDetailsBinding(),
    ),
    GetPage(
      name: map_view_route,
      page: () => MapViewPage(),
      binding: MapviewBinding(),
    ),
    GetPage(
      name: notification_route,
      page: () => const NotificanPage(),
      binding: NotificationBinding(),
    ),
    GetPage(
      name: qr_scanner,
      page: () => QRViewExample(),
      binding: QrBinding(),
    ),
  ];
}
