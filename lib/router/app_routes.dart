const String splashRoute = '/';
const String loginRoute = '/signin';
const String profileRoute = '/signup';

const String RESET_PASSWORD = '/reset_password';
const String OTP = '/otp';
const String change_password_route = '/change_password';
const String dashboardRoute = '/dash_board';
const String MY_ORDERS = '/my_orders';
const String MY_ORDERS_DETAIL = '/my_orders_detail';
const String map_view_route = '/map_view_route';
const String notification_route = '/notification_route';
const String qr_scanner = '/qr_scanner';
