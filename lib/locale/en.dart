const en_US = {
  'counter_text': 'You have pushed the button this many times',
  'english': 'English',
  'arabic': 'العربي',
  'counter_page_title': 'Settings',
  'scheduled': 'Scheduled',
  'pending': 'Pending',
  'delivered': 'Delivered',
};
