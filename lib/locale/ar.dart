const ar_DZ = {
  'counter_text': 'Umebonyeza kitufe mara:',
  'english': 'English',
  'arabic': 'العربي',
  'counter_page_title': 'Mfano wa GetX wa kuongeza namba',
  'scheduled': 'المقرر',
  'pending': 'قيد الانتظار',
  'delivered': 'تم التوصيل'
};
