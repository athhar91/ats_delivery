import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_delivery_app/data/repository/product_service.dart';
import 'package:flutter_delivery_app/util/utils.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '../data/model/Responces/push_models.dart';
import '../resources/app_colors.dart';

class FCMHelper {
  static final FirebaseMessaging _firebaseMessaging =
      FirebaseMessaging.instance;
  static FlutterLocalNotificationsPlugin? _flutterLocalNotificationsPlugin =
      null;

  static registerFCMToken() async {
    var token = await _firebaseMessaging.getToken();
    print('tokenid :$token');
    await Api().tokenRegister(token!);
  }

  static configureFCM(BuildContext context) async {
    iOS_Permission();
    RemoteMessage? message = await _firebaseMessaging.getInitialMessage();
    if (message != null) {
      handleRemoteMessageClick(message, context);
    }
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      if (message != null) handleRemoteMessageClick(message, context);
    });
    FirebaseMessaging.onMessage.listen((message) {
      if (message != null) handleRemoteMessageReceived(message, context);
    });
  }

  static handleRemoteMessageClick(RemoteMessage message, BuildContext context) {
    if (message != null) {
      var push = PushModel.fromMap(message.data);
      if (push.push_order_id.isNotEmpty) {}
    }
  }

  static handleRemoteMessageReceived(
      RemoteMessage message, BuildContext context) {
    if (message.data != null) {
      RemoteNotification? notification = message.notification;

      print("message.data" + notification!.title.toString());
      if (_flutterLocalNotificationsPlugin == null)
        configureLocalNotification();
      var push = PushModel.fromMap(message.data);
      var androidPlatformChannelSpecifics = AndroidNotificationDetails(
          'Ifada', 'Local Notification',
          importance: Importance.max,
          priority: Priority.high,
          ticker: 'ticker',
          color: AppColor.appBlue);
      var iOSPlatformChannelSpecifics = IOSNotificationDetails();
      var platformChannelSpecifics = NotificationDetails(
          android: androidPlatformChannelSpecifics,
          iOS: iOSPlatformChannelSpecifics);
      _flutterLocalNotificationsPlugin!.show(getInt(push.push_order_id),
          push.title, push.message, platformChannelSpecifics,
          payload: message.data.toString());
    }
  }

  static configureLocalNotification() {
    _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('launcher_icon');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
            onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS);
    _flutterLocalNotificationsPlugin!.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  static void iOS_Permission() {
    _firebaseMessaging.requestPermission(sound: true, badge: true, alert: true);
  }

  static Future onSelectNotification(String? payload) async {
    if (payload != null && payload.isNotEmpty) {}
  }

  static Future onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    if (payload != null && payload.isNotEmpty) {}
  }
}
