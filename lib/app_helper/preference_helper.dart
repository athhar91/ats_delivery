import 'package:flutter_delivery_app/data/model/Responces/users.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../data/repository/product_service.dart';

class PreferenceHelper {
  static SharedPreferences? sharedPref;
  Api? api;
  static Future<SharedPreferences?> getPref() async {
    if (sharedPref != null) return sharedPref;
    return await SharedPreferences.getInstance();
  }

  static void saveUserData(Users user) async {
    // AppDataHelper.user = user;
    var pref = await getPref();

    user.save(pref!);
  }

  static Future<Users> getUserData() async {
    Users users;
    var pref = await getPref();
    users = Users.getUserFromPref(pref!);
    return users;
  }

  // static void saveLanguage(String lang) async {
  //   if (lang != null && lang.isNotEmpty) {
  //     AppDataHelper.lang = lang;
  //     if (AppDataHelper.defaultValues != null)
  //       AppDataHelper.defaultValues.language_code = lang;
  //     var pref = await getPref();
  //     pref.setString("defaultlang", lang);
  //   }
  // }

  // static Future<String> getLanguage() async {
  //   var pref = await getPref();
  //   var lang = pref.getString("defaultlang");
  //   if (AppDataHelper.defaultValues != null && lang != null)
  //     AppDataHelper.defaultValues.language_code = lang;
  //   AppDataHelper.lang = lang ?? "ar";
  //   return lang;
  // }

  // static void saveCountryData(Country country,
  //     {bool isSelected = false}) async {
  //   AppDataHelper.country = country;
  //   var pref = await getPref();
  //   country.save(pref, isSelected);
  // }

  // static Future<Country> getCountryData() async {
  //   var pref = await getPref();
  //   AppDataHelper.country = Country.getCountryFromPref(pref);
  //   return AppDataHelper.country;
  // }

  static clearUser() async {
    var pref = await getPref();
    Users.clearUser(pref!);
    // AppDataHelper.user = null;
  }

  // static void saveToken(String token) async {
  //   AppDataHelper.token = token;
  //   var pref = await getPref();
  //   pref.setString("u_token", token);
  // }

  static Future<String?> getId() async {
    var pref = await getPref();
    return pref!.getString("id");
  }

  static Future<String?> getType() async {
    var pref = await getPref();
    return pref!.getString("user_type");
  }

  static Future<String?> getCountryId() async {
    var pref = await getPref();
    return pref!.getString("country_id");
  }
}
