import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/dashboad_page/controllers/dashboard_controller.dart';
import 'package:get/get.dart';

import '../resources/app_colors.dart';
import '../router/app_routes.dart';

class DialogHelper {
  static GlobalKey<State> key = GlobalKey<State>();

  static Color themeColor = Colors.red;
  static LinearGradient linearGradient = const LinearGradient(
    colors: <Color>[Colors.yellowAccent, Colors.red, Colors.purple],
  );
  static Shader gradientShader = const LinearGradient(
    colors: <Color>[Colors.yellowAccent, Colors.red, Colors.purple],
  ).createShader(const Rect.fromLTWH(0.0, 0.0, 400.0, 1000.0));
  static String logo = 'assets/heart.png';

  get callBack => null;

  // static String value =
  //     AppDataHelper.defaultValues.language_code == "ar" ? "Arabic" : "English";

  // Function callBack;

  // DialogHelper({this.callBack});

  void showAlert(String message, BuildContext context,
      {String? title, String? content, String yesButton = ""}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(top: 20, left: 15, right: 15),
                child: Text(
                  title!,
                  style: TextStyle(
                    color: DialogHelper.themeColor,
                    fontFamily: 'Product Sans',
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 15, right: 15, left: 15),
                child: Text(
                  message,
                  style: const TextStyle(
                    color: Colors.black54,
                    fontFamily: 'Product Sans',
                    fontSize: 15.0,
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                alignment: Alignment.bottomRight,
                child: Row(
                  children: [
                    Expanded(child: Container()),
                    callBack != null
                        ? InkWell(
                            splashColor: Colors.transparent,
                            focusColor: Colors.transparent,
                            highlightColor: Colors.transparent,
                            child: Container(
                              margin: const EdgeInsets.only(
                                  right: 10, bottom: 10, left: 10),
                              child: Text(
                                "proceed",
                                style: TextStyle(
                                  color: DialogHelper.themeColor,
                                  fontFamily: 'Product Sans',
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            onTap: () {
                              Navigator.of(context, rootNavigator: true).pop();
                              if (callBack != null) callBack();
                            },
                          )
                        : Container(),
                    InkWell(
                      splashColor: Colors.transparent,
                      focusColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      child: Container(
                        margin: const EdgeInsets.only(
                            right: 10, bottom: 10, left: 15),
                        child: Text(
                          callBack != null ? 'close' : 'ok',
                          style: TextStyle(
                            color: DialogHelper.themeColor,
                            fontFamily: 'Product Sans',
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context, rootNavigator: true).pop();
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  // static Future<void> showLoadingDialog(
  //     BuildContext context, bool dismissible) async {
  //   return showDialog<void>(
  //       context: context,
  //       barrierDismissible: dismissible,
  //       builder: (BuildContext context) {
  //         return WillPopScope(
  //             onWillPop: () async => dismissible,
  //             child: Center(
  //                 child: SpinKitCircle(
  //               color: AppColor.appBlue,
  //               size: 90,
  //             )));
  //       });
  // }

  static void closeLoadingDialog({context = BuildContext}) {
    if (key.currentContext != null) {
      Navigator.of(key.currentContext!).pop();
    } else if (context != null) {
      Navigator.of(context).pop();
    }
  }

  static void showLoginAlert({String message = "Please Login to Proceed"}) {}

  // static void showCountrySelection(
  //     BuildContext context, ValueChanged<Country> onChanged) {
  //   showModalBottomSheet(
  //       context: context,
  //       barrierColor: Colors.black38,
  //       backgroundColor: Colors.white,
  //       shape: RoundedRectangleBorder(
  //           borderRadius: BorderRadius.vertical(top: Radius.circular(30.0))),
  //       builder: (BuildContext context) {
  //         return Container(
  //           padding: EdgeInsets.all(20),
  //           //decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.only(topLeft: Radius.circular(40.0),topRight: Radius.circular(40.0))),
  //           height: 300,
  //           child: Center(
  //             child: ListView.builder(
  //                 itemCount: AppDataHelper.countries.length,
  //                 itemBuilder: (context, index) {
  //                   return InkWell(
  //                     child: Container(
  //                         padding: EdgeInsets.all(10),
  //                         child: Row(children: [
  //                           Image.asset(
  //                             AppDataHelper.countries[index].getFlagFromAsset(),
  //                             height: 20,
  //                             width: 20,
  //                           ),
  //                           Expanded(
  //                             child: Container(
  //                                 padding: EdgeInsets.only(left: 20),
  //                                 child: Text(
  //                                     AppDataHelper.countries[index].name)),
  //                             flex: 1,
  //                           ),
  //                           Visibility(
  //                             child: Image.asset(
  //                               "assets/tick_black.png",
  //                               height: 20,
  //                               width: 20,
  //                               color: Colors.blue,
  //                             ),
  //                             visible: AppDataHelper.country.id ==
  //                                 AppDataHelper.countries[index].id,
  //                           ),
  //                         ])),
  //                     onTap: () {
  //                       onChanged(AppDataHelper.countries[index]);
  //                       Navigator.pop(context);
  //                     },
  //                   );
  //                 }),
  //           ),
  //         );
  //       });
  // }

  // static void showCountryCodePicker(BuildContext context, String selected,
  //     ValueChanged<CountryCode> onChanged) {
  //   showModalBottomSheet(
  //       context: context,
  //       barrierColor: Colors.transparent,
  //       backgroundColor: Colors.white,
  //       shape: RoundedRectangleBorder(
  //           borderRadius: BorderRadius.vertical(top: Radius.circular(30.0))),
  //       builder: (BuildContext context) {
  //         var countries = CountryCode.getCountries();
  //         return Container(
  //           padding: EdgeInsets.all(20),
  //           //decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.only(topLeft: Radius.circular(40.0),topRight: Radius.circular(40.0))),
  //           height: 300,
  //           child: Center(
  //             child: ListView.builder(
  //                 itemCount: countries.length,
  //                 itemBuilder: (context, index) {
  //                   return InkWell(
  //                     child: Container(
  //                         padding: EdgeInsets.all(10),
  //                         child: Row(children: [
  //                           Image.asset(
  //                             countries[index].flag,
  //                             height: 20,
  //                             width: 20,
  //                           ),
  //                           Container(
  //                             padding: EdgeInsets.only(left: 20),
  //                             child: Text(countries[index].cc),
  //                             width: 100,
  //                           ),
  //                           Expanded(
  //                             child: Text(countries[index].country),
  //                             flex: 1,
  //                           ),
  //                           Visibility(
  //                             child: Image.asset(
  //                               "assets/tick_black.png",
  //                               height: 20,
  //                               width: 20,
  //                               color: Colors.blue,
  //                             ),
  //                             visible: selected == countries[index].cc,
  //                           ),
  //                         ])),
  //                     onTap: () {
  //                       onChanged(countries[index]);
  //                       Navigator.pop(context);
  //                     },
  //                   );
  //                 }),
  //           ),
  //         );
  //       });
  // }

  // Popup for Logout

  static logoutAlertDialog(BuildContext context, content) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Container(child: content),
          );
        });
  }

  static showLoginSignupAlert(BuildContext context,
      {String message = "Please Login or Signup to Proceed"}) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Container(
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(5)),
              child: SizedBox(
                height: 120,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: const [
                        /*Container(
                          height: 30,
                          width: double.infinity,
                          child: Text(
                            AppLocalizations.of(context).translate('login_required'),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                decoration: TextDecoration.none,
                                //Removes the yellow line
                                color: AppColor.appViolet,
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.w500,
                                fontSize: 18),
                          ),
                        ),*/
                        Text(
                          "Please login",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              fontFamily: "Poppins"),
                        ),
                      ],
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            height: 30,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: AppColor.appBlue),
                            child: RaisedButton(
                              splashColor: AppColor.appBlue,
                              color: AppColor.appViolet,
                              onPressed: () {
                                //Navigator.push(context, route);
                                Get.toNamed(loginRoute);
                              },
                              child: Row(
                                children: const [
                                  Text(
                                    'login',
                                    style: TextStyle(
                                        decoration: TextDecoration.none,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        fontFamily: "Poppins",
                                        color: Colors.black),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          /* Container(
                            height: 30,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: AppColor.appBlue),
                            child: RaisedButton(
                                splashRouteColor: AppColor.appBlue,
                                color: Colors.white,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: FlatButton(
                                  minWidth: 50,
                                  onPressed: () {
                                    NavigationHelper.openScreen(
                                        context, NavigationHelper.signupRote);
                                  },
                                  child: Row(
                                    children: [
                                      Text(
                                        " Signup",
                                        style: TextStyle(
                                            decoration: TextDecoration.none,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                            fontFamily: "Poppins",
                                            color: Colors.black87),
                                      ),
                                    ],
                                  ),
                                )),
                          ),*/
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  static void showAlertDialog(
      String title, String content, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          title: Text(
            title,
            //textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w500,
            ),
          ),
          content: Text(content),
          actions: <Widget>[
            Center(
              child: ElevatedButton(
                style: ButtonStyle(
                    alignment: Alignment.center,
                    backgroundColor: MaterialStateProperty.all<Color>(
                        AppColor.appPrimaryColor),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                      //side: BorderSide(color: Colors.red)
                    ))),
                child: const Text(
                  'ok',
                ),
                onPressed: () {
                  if (content == 'Order Status is Updated') {
                    Get.find<DashBoardController>().callFunctions();

                    Get.offAllNamed(dashboardRoute);
                  } else {
                    Navigator.of(context, rootNavigator: true).pop();
                  }

                  // if (title ==
                  //     AppLocalizations.of(context).translate('address')) {
                  //   //NavigationHelper.launchNewScreen(context,NavigationHelper.myCartroute);
                  //   NavigationHelper.launchNewScreen(
                  //       context, NavigationHelper.homeScreen,
                  //       args: HashMap.from({"currentPage": 1}));
                  // }
                },
              ),
            ),
          ],
        );
      },
    );
  }

  // static void showAlertAddressDialog(
  //     String title, String content, BuildContext context) {
  //   showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return AlertDialog(
  //         shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.all(Radius.circular(20.0))),
  //         title: new Text(
  //           title,
  //           //textAlign: TextAlign.center,
  //           style: TextStyle(
  //             fontSize: 20,
  //             fontWeight: FontWeight.w500,
  //           ),
  //         ),
  //         content: new Html(
  //           data: content,
  //         ),
  //         actions: <Widget>[
  //           Center(
  //             child: ElevatedButton(
  //               style: ButtonStyle(
  //                   alignment: Alignment.center,
  //                   backgroundColor:
  //                       MaterialStateProperty.all<Color>(AppColor.appViolet),
  //                   shape: MaterialStateProperty.all<RoundedRectangleBorder>(
  //                       RoundedRectangleBorder(
  //                     borderRadius: BorderRadius.circular(20),
  //                     //side: BorderSide(color: Colors.red)
  //                   ))),
  //               child: new Text(
  //                 AppLocalizations.of(context).translate('ok'),
  //               ),
  //               onPressed: () {
  //                 Navigator.of(context, rootNavigator: true).pop();
  //                 if (title ==
  //                     AppLocalizations.of(context).translate('address')) {
  //                   //NavigationHelper.launchNewScreen(context,NavigationHelper.myCartroute);
  //                   NavigationHelper.launchNewScreen(
  //                       context, NavigationHelper.homeScreen,
  //                       args: HashMap.from({"currentPage": 2}));
  //                 }
  //               },
  //             ),
  //           ),
  //         ],
  //       );
  //     },
  //   );
  // }

  // static void showCongratulateDialog(
  //     String title, String content, BuildContext context) {
  //   showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return AlertDialog(
  //         shape: RoundedRectangleBorder(
  //             borderRadius: BorderRadius.all(Radius.circular(20.0))),
  //         title: new Text(
  //           title,
  //           textAlign: TextAlign.center,
  //           style: TextStyle(
  //             fontSize: 20,
  //             fontWeight: FontWeight.w500,
  //           ),
  //         ),
  //         content: new Html(
  //           data: content,
  //         ),
  //         actions: <Widget>[
  //           Center(
  //             child: ElevatedButton(
  //               style: ButtonStyle(
  //                   alignment: Alignment.center,
  //                   backgroundColor:
  //                       MaterialStateProperty.all<Color>(AppColor.appViolet),
  //                   shape: MaterialStateProperty.all<RoundedRectangleBorder>(
  //                       RoundedRectangleBorder(
  //                     borderRadius: BorderRadius.circular(20),
  //                     //side: BorderSide(color: Colors.red)
  //                   ))),
  //               child: new Text(
  //                 "",
  //               ),
  //               onPressed: () {
  //                 Navigator.of(context, rootNavigator: true).pop();
  //               },
  //             ),
  //           ),
  //         ],
  //       );
  //     },
  //   );
  // }

  static void showCreditAlertDialog(BuildContext context, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: SizedBox(
            height: 120,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.of(context, rootNavigator: true).pop();
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.black87,
                        ),
                        child: const Icon(
                          Icons.cancel_rounded,
                          color: Colors.white,
                          size: 18,
                        ),
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  width: 45,
                  height: 45,
                  child: Icon(
                    Icons.warning_rounded,
                    color: Colors.red,
                    size: 50,
                  ),
                ),
                Container(
                  child: Text(
                    content,
                    style: const TextStyle(fontSize: 12),
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  // static void showLanguageSelection(
  //     BuildContext context, ValueChanged<String> onChanged) {
  //   print("showlanguage");
  //   print(value);
  //   showModalBottomSheet(
  //       context: context,
  //       barrierColor: Colors.black38,
  //       backgroundColor: Colors.white,
  //       shape: RoundedRectangleBorder(
  //           borderRadius: BorderRadius.vertical(top: Radius.circular(30.0))),
  //       builder: (BuildContext context) {
  //         return Container(
  //           margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
  //           child: Wrap(
  //             //crossAxisAlignment: CrossAxisAlignment.start,
  //             children: [
  //               InkWell(
  //                 onTap: () {
  //                   onChanged("English");
  //                   Navigator.pop(context);
  //                   value = "English";
  //                 },
  //                 child: Row(
  //                   children: [
  //                     Expanded(
  //                       child: Container(
  //                         margin: EdgeInsets.symmetric(
  //                             horizontal: 20, vertical: 10),
  //                         child: Text(
  //                           "English",
  //                           style: TextStyle(
  //                               color: Colors.black,
  //                               fontWeight: FontWeight.w600,
  //                               fontSize: 15),
  //                         ),
  //                       ),
  //                     ),
  //                     Visibility(
  //                       child: Image.asset(
  //                         "assets/tick_black.png",
  //                         height: 20,
  //                         width: 20,
  //                         color: AppColor.appViolet,
  //                       ),
  //                       visible: value == "English",
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //               InkWell(
  //                 onTap: () {
  //                   onChanged("Arabic");
  //                   Navigator.pop(context);
  //                   value = "Arabic";
  //                 },
  //                 child: Row(
  //                   children: [
  //                     Expanded(
  //                       child: Container(
  //                         margin: EdgeInsets.symmetric(
  //                             horizontal: 20, vertical: 10),
  //                         child: Text(
  //                           "العربية",
  //                           style: TextStyle(
  //                               color: Colors.black,
  //                               fontWeight: FontWeight.w600,
  //                               fontSize: 15),
  //                         ),
  //                       ),
  //                     ),
  //                     Visibility(
  //                       child: Image.asset(
  //                         "assets/tick_black.png",
  //                         height: 20,
  //                         width: 20,
  //                         color: AppColor.appViolet,
  //                       ),
  //                       visible: value == "Arabic",
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //             ],
  //           ),
  //         );
  //       });
  // }

  // void getEmailAlert(BuildContext context) {
  //   var reason = "";
  //   showModalBottomSheet(
  //       context: context,
  //       isScrollControlled: true,
  //       barrierColor: Colors.black38,
  //       backgroundColor: Colors.white,
  //       shape: RoundedRectangleBorder(
  //         borderRadius: BorderRadius.circular(10),
  //       ),
  //       builder: (BuildContext context) {
  //         return Container(
  //           height: 200,
  //           child: Column(
  //             mainAxisAlignment: MainAxisAlignment.spaceAround,
  //             children: [
  //               Row(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 children: [
  //                   Text(
  //                     "Please enter your email to continue",
  //                     style: TextStyle(
  //                         color: Colors.black,
  //                         fontSize: 16,
  //                         fontWeight: FontWeight.w600),
  //                   )
  //                 ],
  //               ),
  //               Container(
  //                   margin: EdgeInsets.symmetric(horizontal: 30),
  //                   decoration: BoxDecoration(
  //                     border: Border.all(
  //                         style: BorderStyle.solid,
  //                         color: AppColor.appBlue,
  //                         width: 2),
  //                     color: Colors.white,
  //                   ),
  //                   child: Row(
  //                     mainAxisAlignment: MainAxisAlignment.center,
  //                     children: [
  //                       Expanded(
  //                         child: Container(
  //                           margin: EdgeInsets.symmetric(horizontal: 10),
  //                           child: Center(
  //                             child: TextEditWidget(
  //                               onChanged: (String value) {
  //                                 reason = value;
  //                               },
  //                               color: AppColor.darkText,
  //                               hintColor: AppColor.bodyText,
  //                               lineColor: AppColor.shadowColor,
  //                               fontSize: 14,
  //                               hintText: "Email",
  //                               value: reason,
  //                               hintSize: 14,
  //                             ),
  //                           ),
  //                         ),
  //                       ),
  //                     ],
  //                   )),
  //               Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                 children: [
  //                   InkWell(
  //                     onTap: () {
  //                       if (callBack != null) {
  //                         AppDataHelper.user.email = reason;
  //                         callBack();
  //                       }
  //                       ;
  //                     },
  //                     child: Container(
  //                       padding: EdgeInsets.all(10),
  //                       decoration: BoxDecoration(color: AppColor.appBlue),
  //                       child: Text(
  //                         "Submit",
  //                         style: TextStyle(
  //                             color: Colors.white,
  //                             fontSize: 14,
  //                             fontWeight: FontWeight.w500),
  //                       ),
  //                     ),
  //                   ),
  //                   // InkWell(
  //                   //   onTap: () {
  //                   //     // widget.onBackPressed(forceBack: true);
  //                   //     DialogHelper.showAlertDialog("Cancel Failed",
  //                   //         "Try again after some time", context);
  //                   //   },
  //                   //   child: Container(
  //                   //     padding: EdgeInsets.all(10),
  //                   //     decoration: BoxDecoration(color: AppColor.appBlue),
  //                   //     child: Text(
  //                   //       "Cancel",
  //                   //       style: TextStyle(
  //                   //           color: Colors.white,
  //                   //           fontSize: 14,
  //                   //           fontWeight: FontWeight.w500),
  //                   //     ),
  //                   //   ),
  //                   // ),
  //                 ],
  //               )
  //             ],
  //           ),
  //         );
  //       });
  // }

  // static void showSnackBar(BuildContext context) {
  //   final snackBar = SnackBar(
  //     margin: EdgeInsets.only(
  //         bottom: SizeConfig.blockSizeHorizontal * 25, right: 20, left: 20),
  //     content: Text("0"),
  //     backgroundColor: AppColor.appViolet,
  //     behavior: SnackBarBehavior.floating,
  //     duration: const Duration(seconds: 120),
  //     action: SnackBarAction(
  //         label: 'View Cart',
  //         textColor: Colors.white,
  //         onPressed: () {
  //           Scaffold.of(context).removeCurrentSnackBar();
  //           NavigationHelper.launchNewScreen(
  //               context, NavigationHelper.homeScreen,
  //               args: HashMap.from({"currentPage": 2}));
  //         }),
  //   );

  //   Future(() => ScaffoldMessenger.of(context).showSnackBar(snackBar));
  // }
}
