import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class AppColor {
  static Color bg = const Color(0xffF4F4F4);
  static Color appYellow = const Color(0xfff8cb00);
  //static Color appBlue = AppColor.appViolet;
  static Color appBlue = HexColor('#002F87');
  static Color appPrimaryColor = HexColor('#273a68');
  static Color appPrimaryColorDark = HexColor('#172a57');

  static Color darkText = const Color(0xff001833);
  static Color bodyText = const Color(0xff8D8F92);
  static Color lineColor = const Color(0xff9E9E9E);
  static Color shadowColor = const Color(0xcdeeeeee);
  static Color appPurple = const Color(0xff51284f);

  static Color appGold = const Color(0xffc1a01f);
  static Color appWhite = const Color(0xffffffff);

  static Color appViolet = HexColor('#D6006D');
  static Color borderBlue = HexColor('#002F87');
  static Color kSecondaryColor = const Color(0xFF979797);
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
