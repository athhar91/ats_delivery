int getInt(String text) {
  if (text.trim().isEmpty) return 0;
  var myInt = int.tryParse(text);
  return myInt != null ? myInt : 0;
}
