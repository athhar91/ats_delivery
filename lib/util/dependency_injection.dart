import 'package:get/get.dart';

import '../data/services/network_services.dart';

class DependencyInjection {
  static void init() async {
    //services
    Get.put<NetworkServices>(NetworkServices(), permanent: true);
  }
}
