import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/router/app_modules.dart';
import 'package:flutter_delivery_app/theme/theme.dart';
import 'package:flutter_delivery_app/theme/themeService.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'locale/app_translation.dart';
import 'firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

Future<void> main() async {
  GetStorage.init();
  // DependencyInjection.init(); //Here we are calling the Dependency Injection

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        navigatorKey: Get.key,
        translations: AppTranslations(),
        locale: Get.deviceLocale,
        debugShowCheckedModeBanner: false,
        title: 'Delivery app',
        theme: Themes().lightTheme,
        darkTheme: Themes().darkTheme,
        themeMode: ThemeService().theme,
        initialRoute: '/',
        builder: EasyLoading.init(),
        getPages: AppPages.routes);
  }
}
