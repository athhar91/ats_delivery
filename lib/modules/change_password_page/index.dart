import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/change_password_page/controllers/change_password_controller.dart';
import 'package:flutter_delivery_app/modules/change_password_page/forms/change_password_form.dart';
import 'package:flutter_delivery_app/resources/app_colors.dart';
import 'package:get/get.dart';

class ChangePasswordPage extends GetView<ChangePasswordController> {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Change Password'),
          backgroundColor: AppColor.appPrimaryColor,
        ),
        body: view(context));
  }

  Column view(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
          child: Container(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                // Text(
                //   'Change password',
                //   style: Theme.of(context)
                //       .primaryTextTheme
                //       .headline5!
                //       .copyWith(color: Theme.of(context).primaryColor),
                // ),
              ])),
        ),
        Center(
            child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                child: const ChangePasswordForm()))
      ],
    );
  }
}
