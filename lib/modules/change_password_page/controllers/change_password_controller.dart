import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/data/repository/product_service.dart';
import 'package:flutter_delivery_app/router/app_routes.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

class ChangePasswordController extends GetxController with StateMixin {
  final formKey = GlobalKey<FormBuilderState>();

  void navigateBack() => Get.offAllNamed(loginRoute);

  void navigateToSignin() =>
      Get.offNamedUntil(loginRoute, (route) => route.isFirst);

  Future<void> submitNewPass() async {
    EasyLoading.show(status: 'loading...');
    try {
      final res = await Api().submitResetPassword();

      if (res.responseStatus != 'error') {
        EasyLoading.dismiss();

        change(null, status: RxStatus.success());
        navigateToSignin();
      } else {
        EasyLoading.dismiss();
      }
    } finally {
      EasyLoading.dismiss();
    }
  }
}
