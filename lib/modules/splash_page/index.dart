import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'app_controllers/app_controller.dart';

class SplashPage extends GetWidget<AppController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GetBuilder<AppController>(
            init: controller.initApp(),
            builder: (_) => Center(
                  child: SizedBox(
                    height: double.infinity,
                    width: double.infinity,
                    child: Image.asset(
                      'assets/images/splash_bg.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                )));
  }
}
