import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/data/model/Responces/users.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../app_helper/fcm_helpers.dart';
import '../../../app_helper/preference_helper.dart';
import '../../../modules/dashboad_page/models/count.dart';
import '../../../router/app_routes.dart';
import '../../../shared/widgets/modals/language_options.dart';

class AppController extends GetxController {
  final count = Count(number: 0).obs;
  final isBusy = false.obs;
  Users? users;
  GetStorage box = GetStorage();

  final tabIndex = 0.obs;

  setTabIndex(int value) {
    tabIndex(value);
  }

  setIsBusy(bool value) {
    isBusy(value);
  }

  initApp() {
    Future.delayed(const Duration(seconds: 1), () async {
      initLocale();
      goToNextPage();
    });
  }

  void goToNextPage() async {
    // await Firebase.initializeApp();

    // FCMHelper.configureFCM(Get.overlayContext!);

    await Future.delayed(const Duration(seconds: 1));

    users = await PreferenceHelper.getUserData();
    if (users!.lastLogin != null) {
      Get.offNamed(dashboardRoute);
    } else {
      Get.offNamed(loginRoute);
    }
  }

  initLocale() {
    GetStorage box = GetStorage();
    if (box.read('locale') != null) {
      box.read('locale') == 'ar_DZ'
          ? Get.updateLocale(const Locale('ar', 'DZ'))
          : Get.updateLocale(const Locale('en', 'US'));
    }
  }

  changeLanguageModal() {
    Get.bottomSheet(const LanguageOptionsWidget());
  }

  updateLocale(Locale locale) {
    print('${locale.languageCode}_${locale.countryCode}');
    GetStorage box = GetStorage();
    box.write('locale', '${locale.languageCode}_${locale.countryCode}');
    Get.updateLocale(locale);
    Get.back();
  }
}
