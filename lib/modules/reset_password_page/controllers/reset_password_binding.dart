import 'package:get/get.dart';
import 'package:flutter_delivery_app/modules/reset_password_page/controllers/reset_password_controller.dart';

class ResetPasswordBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ResetPasswordController>(() => ResetPasswordController());
  }
}
