import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'mapview_controller.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapViewPage extends GetView<MapviewController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('MapviewPage'),
        centerTitle: true,
      ),
      body: GoogleMap(
        initialCameraPosition: controller.kGooglePlex!,
        onMapCreated: (GoogleMapController controller1) {
          controller.mapController.complete(controller1);
        },
      ),
    );
  }
}
