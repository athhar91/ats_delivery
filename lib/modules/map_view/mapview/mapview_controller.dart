import 'dart:async';

import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapviewController extends GetxController {
  final mapController = Completer<GoogleMapController>();
  CameraPosition? kGooglePlex;
  //TODO: Implement MapviewController.

  @override
  void onInit() {
    kGooglePlex = const CameraPosition(
      target: LatLng(37.42796133580664, -122.085749655962),
      zoom: 14.4746,
    );
    super.onInit();
  }


}
