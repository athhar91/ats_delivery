import 'package:get/get.dart';

import 'mapview_controller.dart';

class MapviewBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MapviewController>(
      MapviewController.new,
    );
  }
}
