import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/dashboad_page/Components/icon_btn_with_counter.dart';
import 'package:flutter_delivery_app/router/app_routes.dart';
import 'package:get/get.dart';

import '../../../resources/size_config.dart';
import '../controllers/dashboard_controller.dart';

class HomeHeader extends GetView<DashBoardController> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // SearchField(),
          IconBtnWithCounter(
            svgSrc: "assets/icons/drawer_menu.svg",
            press: () => controller.handleMenuButtonPressed(),
          ),
          IconBtnWithCounter(
            svgSrc: "assets/icons/Bell.svg",
            numOfitem: 3,
            press: () {
              Get.toNamed(notification_route);
            },
          ),
        ],
      ),
    );
  }
}
