import 'package:flutter/material.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:flutter_delivery_app/app_helper/fcm_helpers.dart';
import 'package:flutter_delivery_app/data/model/Responces/myOrders.dart';
import 'package:flutter_delivery_app/data/model/Responces/users.dart';
import 'package:flutter_delivery_app/router/app_routes.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:get/get.dart';
import 'package:whatsapp_unilink/whatsapp_unilink.dart';
import '../../../app_helper/preference_helper.dart';
import '../../../data/model/Responces/dashboard_reposnce.dart';
import '../../../shared/widgets/modals/language_options.dart';
import '../repository/dashboard_repository.dart';

class DashBoardController extends GetxController with StateMixin {
  final dashBoardRepository = DashBoardRepository();
  final advancedDrawerController = AdvancedDrawerController();
  var data = Data().obs;
  var orders = <Order>[].obs;
  var ordersDelivered = <Order>[].obs;
  var ordersOutForDelivery = <Order>[].obs;
  var users = Users().obs;
  final firstName = "".obs;
  final email = "".obs;
  final pendingCount = 0.obs;
  final deliveryCount = 0.obs;
  final dateController = TextEditingController();
  DateTime selectedDate = DateTime.now();
  DashboardReposnce? dashboardReposnce;
  final GlobalKey<RefreshIndicatorState> refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  RxDouble deliveredPieCount = 0.0.obs;
  RxDouble pendingPieCount = 0.0.obs;
  RxDouble outForDeliveryPieCount = 0.0.obs;

  // final dataMap = <String, double>{
  //   "Delivered": 0,
  //   "Pending": 0,
  //   "Out For Delivery": 0
  // };

  Map<String, double> dataMap = {
    "Delivered": 0,
    "Pending": 0,
    "Out For Delivery": 0
  };

  final colorList = <Color>[
    Colors.greenAccent,
    Colors.redAccent,
    Colors.blueAccent,
  ];

  @override
  void onInit() {
    // getDashBoardData();
    callFunctions();
    print("adsads${data.value}");

    super.onInit();
  }

  void callFunctions() {
    FCMHelper.registerFCMToken();
    getDashBoardData();
    getorderList('3');
    getorderList('5');
    getorderList('4');

    getUsers();
  }

  Future<void> getDashBoardData() async {
    change(null, status: RxStatus.loading());

    try {
      final res = await dashBoardRepository.getDashBoardData();
      if (res != null) {
        // EasyLoading.dismiss();
        change(null, status: RxStatus.success());
        dashboardReposnce = res;
        deliveryCount.value = dashboardReposnce!.data!.deliveryCount!;
        pendingCount.value = dashboardReposnce!.data!.pendingCount!;
      }
    } finally {}
  }

  Future<void> getorderList(String type) async {
    change(null, status: RxStatus.loading());

    try {
      final res = await dashBoardRepository.getPendingOrder(type);
      if (res.data != null) {
        change(null, status: RxStatus.success());

        data.value = res.data!;
        print("res.data${orders.value.isEmpty}");

        if (type == '3') {
          orders.value = data.value.orders!;
        } else if (type == '4') {
          ordersOutForDelivery.value = data.value.orders!;
        } else {
          ordersDelivered.value = data.value.orders!;

          // pendingorder();

          // print(orders.value.length);
          // print(ordersDelivered.value.length);
          // print(ordersOutForDelivery.value.length);

          // print(orders.value.length +
          //     ordersDelivered.value.length +
          //     ordersOutForDelivery.value.length);
        }

        pendingPieCount.value = orderPercentage(orders.length);
        outForDeliveryPieCount.value =
            orderPercentage(ordersOutForDelivery.length);
        deliveredPieCount.value = orderPercentage(ordersDelivered.length);
        update();
      }
    } finally {}
  }

  void getUsers() async {
    users.value = await PreferenceHelper.getUserData();
    firstName.value = users.value.firstName!;
    email.value = users.value.email!;

    print('dsaf${users.value.firstName}');
  }

  double orderPercentage(int orderLength) {
    int totalOrders;
    double orderPercentage = 0;
    totalOrders = orders.value.length +
        ordersDelivered.value.length +
        ordersOutForDelivery.value.length;

    orderPercentage = orderLength / totalOrders * 100;
    return orderPercentage;
  }

  void handleMenuButtonPressed() {
    // NOTICE: Manage Advanced Drawer state through the Controller.
    // _advancedDrawerController.value = AdvancedDrawerValue.visible();
    advancedDrawerController.showDrawer();
  }

  // void selectDate(DateTime? selectedDates) async {
  //   dateController.text = DateFormat('yyyy-MM-dd').format(selectedDate);
  // }

  Future<void> makePhoneCall(String phoneNumber) async {
    final Uri launchUri = Uri(scheme: 'tel', path: phoneNumber);
    await launchUrl(launchUri);
  }

  sendWhatsMsg(String phoneNumber) async {
    final link = WhatsAppUnilink(
      phoneNumber: '"966"$phoneNumber',
      text: "Hello..",
    );
    // Convert the WhatsAppUnilink instance to a string.
    // Use either Dart's string interpolation or the toString() method.
    // The "launch" method is part of "url_launcher".
    await launch('$link');
  }

  navigateToMap(double lat, double lng, String address) async {
    MapsLauncher.launchCoordinates(lat, lng, address);
  }

  changeLanguageModal() {
    Get.bottomSheet(const LanguageOptionsWidget());
  }

  void logoutDialog() {
    Get.dialog(AlertDialog(
      title: const Text(''),
      content: const Text('Are you sure you want to log out?'),
      actions: [
        TextButton(
          onPressed: () {},
          child: const Text('CANCEL'),
        ),
        TextButton(
          onPressed: () {
            PreferenceHelper.clearUser();
            Get.offAllNamed(loginRoute);
          },
          child: const Text('ACCEPT'),
        ),
      ],
    ));
  }
}
