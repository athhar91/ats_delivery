import 'package:flutter_delivery_app/modules/my_orders_page.dart/controllers/my_orders_controller.dart';
import 'package:get/get.dart';

import '../../splash_page/app_controllers/app_controller.dart';
import 'dashboard_controller.dart';

class DashBoardBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashBoardController>(() => DashBoardController(), fenix: false);
    Get.lazyPut<MyOrdersController>(() => MyOrdersController(), fenix: false);

    Get.lazyPut<AppController>(() => AppController());
  }
}
