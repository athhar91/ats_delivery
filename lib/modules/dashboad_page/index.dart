import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:flutter_delivery_app/modules/dashboad_page/widgets/NavigationItem.dart';
import 'package:flutter_delivery_app/shared/widgets/tabs/bottom_tabs_navigator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/shared/widgets/tabs/tab_page_switcher.dart';
import 'package:get/get.dart';

import '../../resources/app_colors.dart';
import 'controllers/dashboard_controller.dart';

class DashBoardPage extends GetWidget<DashBoardController> {
  const DashBoardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AdvancedDrawer(
      backdropColor: AppColor.appPrimaryColor,
      controller: controller.advancedDrawerController,
      animationCurve: Curves.easeInOut,
      animationDuration: const Duration(milliseconds: 300),
      animateChildDecoration: true,
      rtlOpening: false,
      openRatio: 6 / 10,
      disabledGestures: false,
      childDecoration: const BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black26,
              blurRadius: 2.0,
              spreadRadius: 3,
              offset: Offset(
                10.0, // Move to right 10  horizontally
                4.0, // Move to bottom 10 Vertically
              ),
            ),
          ],
          color: Colors.white10,
          borderRadius: BorderRadius.all(Radius.circular(16))),
      drawer: SafeArea(
        child: drawerItem(),
      ),
      child: Scaffold(
          body: TabPageSwitcher(),
          bottomNavigationBar: const BottomTabsNavigator()),
    );
  }

  NavigationItem drawerItem() {
    return NavigationItem();
  }
}
