import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/dashboad_page/controllers/dashboard_controller.dart';
import 'package:flutter_delivery_app/resources/app_colors.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../../resources/size_config.dart';
import '../../../router/app_routes.dart';
import '../widgets/listWidget/delivery_list.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:shimmer/shimmer.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import '../../../resources/app_colors.dart';

class HomeTab extends GetView<DashBoardController> {
  const HomeTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx((state) {
      return dashboardTabView();
    },
        onLoading: shimmerLoading(),
        onEmpty: const Center(child: Text('No data found')),
        onError: (error) => Text(error!));
  }

  DefaultTabController dashboardTabView() {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: Colors.grey.withOpacity(.1),
          body:
              // Pull from top to show refresh indicator.
              NestedScrollView(
            headerSliverBuilder: (context, innerBoxIsScrolled) => [
              SliverOverlapAbsorber(
                sliver: SliverSafeArea(
                  top: false,
                  sliver: SliverAppBar(
                    floating: true,
                    pinned: true,
                    snap: true,
                    forceElevated: true,
                    elevation: 1,
                    backgroundColor: Colors.white,
                    expandedHeight: 330.0,
                    collapsedHeight: 58.0,
                    leading: InkWell(
                      onTap: (() => controller.handleMenuButtonPressed()),
                      child: Icon(
                        Icons.menu,
                        color: AppColor.appPrimaryColor,
                      ),
                    ),
                    title: const Text('IFADA DELIVERY'),
                    flexibleSpace: FlexibleSpaceBar(
                      titlePadding: EdgeInsets.only(bottom: Get.width * .30),
                      title: const Text(
                        '',
                        textScaleFactor: .5,
                        style: TextStyle(color: Colors.blue),
                      ),
                      centerTitle: true,
                      background: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16, vertical: 25),
                          child: Obx(
                            () => PieChart(
                              dataMap: <String, double>{
                                "Pending": controller.pendingPieCount.value,
                                "Out For Delivery":
                                    controller.outForDeliveryPieCount.value,
                                "Delivered": controller.deliveredPieCount.value,
                              },
                              chartType: ChartType.disc,
                              baseChartColor:
                                  Colors.grey[50]!.withOpacity(0.15),
                              colorList: controller.colorList,
                              chartValuesOptions: const ChartValuesOptions(
                                  showChartValuesInPercentage: true),
                              totalValue: 100,
                            ),
                          )),
                      // background:
                      //     Image.asset('assets/images/landing_page_bg.png'),
                    ),
                    actions: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.notifications,
                          color: AppColor.appPrimaryColor,
                        ),
                        tooltip: 'Add new entry',
                        onPressed: () {
                          Get.toNamed(notification_route);
                        },
                      ),
                    ],
                    bottom: TabBar(
                      labelColor: AppColor.appBlue,
                      unselectedLabelColor: Colors.grey,
                      indicatorColor: Colors.white,
                      indicatorSize: TabBarIndicatorSize.tab,
                      labelPadding: const EdgeInsets.all(0),
                      labelStyle:
                          const TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                      indicatorPadding: const EdgeInsets.all(0),
                      tabs: [
                        individualTab(context, "pending",
                            controller.pendingCount.value.toString()),
                        individualTab(context, "Out for Delivery", "0"),
                        individualTab(context, "delivered",
                            controller.deliveryCount.value.toString()),

                        // Tab(text: "Pending"),
                        // Tab(text: "Delivered"),
                      ],
                    ),
                  ),
                ),
                handle:
                    NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              ),
            ],
            body: SizedBox(
              // or SizedBox, etc.
              height: Get.width * 14,
              child: TabBarView(children: [
                PendingDelivery(
                  data: controller.orders.value,
                ),
                PendingDelivery(
                  data: controller.ordersOutForDelivery.value,
                ),
                PendingDelivery(
                  data: controller.ordersDelivered.value,
                ),
              ]),
            ),
          ),
          drawer: const Drawer(),
        ));
  }

  SingleChildScrollView shimmerLoading() {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Shimmer.fromColors(
            baseColor: Colors.black26,
            highlightColor: Colors.white,
            child: Container(
              width: double.infinity,
              height: 300,
              color: Colors.black26,
            ),
          ),
          const SizedBox(
            height: 10,
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Shimmer.fromColors(
                baseColor: Colors.black26,
                highlightColor: Colors.white,
                child: Container(
                  width: Get.width * .3,
                  height: Get.width * .08,
                  decoration: BoxDecoration(
                    color: Colors.black26,
                    borderRadius: BorderRadius.circular(50),
                  ),
                ),
              ),
              Shimmer.fromColors(
                baseColor: Colors.black26,
                highlightColor: Colors.white,
                child: Container(
                  width: Get.width * .3,
                  height: Get.width * .08,
                  decoration: BoxDecoration(
                    color: Colors.black26,
                    borderRadius: BorderRadius.circular(50),
                  ),
                ),
              ),
            ],
          ),

          // Container(
          //   height: 140,
          //   margin: EdgeInsets.all(4),
          //   child: ListView(
          //     shrinkWrap: true,
          //     scrollDirection: Axis.horizontal,
          //     children: List.generate(6, (index) {
          //       return ClipRRect(
          //           borderRadius: BorderRadius.all(Radius.circular(16)),
          //           child: Container(
          //             height: 140,
          //             width: 90,
          //             margin: EdgeInsets.all(4),
          //             color: Colors.black26,
          //           ));
          //     }),
          //   ),
          // ),
          SizedBox(
              height: 400,
              child: ListView(
                  physics: const NeverScrollableScrollPhysics(),
                  children: List.generate(4, (childIndex) {
                    return Shimmer.fromColors(
                      baseColor: Colors.black26,
                      highlightColor: Colors.white,
                      child: Container(
                        height: 160,
                        margin: const EdgeInsets.only(
                            left: 10, right: 10, top: 10, bottom: 10),
                        color: Colors.black26,
                      ),
                    );
                  }))),
        ],
      ),
    );
  }
}

Widget individualTab(BuildContext context, String text, String count) {
  return Container(
    height: 60 + MediaQuery.of(context).padding.bottom,
    padding: const EdgeInsets.all(5),
    width: double.infinity,
    decoration: BoxDecoration(
        border: Border(
            right: BorderSide(
              color: Colors.blueGrey.withOpacity(.2),
              width: 1,
              style: BorderStyle.solid,
            ),
            top: BorderSide(
              color: Colors.blueGrey.withOpacity(.2),
              width: .5,
              style: BorderStyle.solid,
            ))),
    child: Center(
      child: Stack(children: [
        Tab(
          text: text.tr,
          icon: text == "pending"
              ? const FaIcon(
                  FontAwesomeIcons.clock,
                  size: 20,
                )
              : text == "delivered"
                  ? const FaIcon(FontAwesomeIcons.box, size: 20)
                  : const FaIcon(FontAwesomeIcons.truck, size: 20),
        ),
        Positioned(
          top: 0.0,
          right: 10.0,
          child: count != "0"
              ? Container(
                  padding: const EdgeInsets.all(1),
                  decoration: BoxDecoration(
                    color: count != "0" ? Colors.red : Colors.white,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  constraints: const BoxConstraints(
                    minWidth: 12,
                    minHeight: 12,
                  ),
                  child: count != "0"
                      ? Text(
                          count,
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 8,
                          ),
                          textAlign: TextAlign.center,
                        )
                      : const Text(''),
                )
              : Container(),
        )
      ]),
    ),
  );
}

class PendingDelivery extends GetView<DashBoardController> {
  List? data;
  PendingDelivery({this.data});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: data.isBlank!
              ? RefreshIndicator(
                  onRefresh: () async {
                    controller.callFunctions();
                  },
                  child: Stack(
                    children: <Widget>[
                      ListView(),
                      const Align(
                        alignment: Alignment.center,
                        child: Text('Data Not Found..',
                            style: TextStyle(color: Colors.grey, fontSize: 22)),
                      )
                    ],
                  ),
                )
              : RefreshIndicator(
                  key: controller.refreshIndicatorKey,
                  color: Colors.white,
                  backgroundColor: Colors.blue,
                  strokeWidth: 4.0,
                  onRefresh: () async {
                    controller.callFunctions();
                  },
                  child: AnimationLimiter(
                    child: Column(
                      children: [
                        Expanded(
                          child: ListView.builder(
                            padding: EdgeInsets.symmetric(
                                horizontal: SizeConfig.horizontal * 2.5),
                            shrinkWrap: true,
                            itemCount: data!.length,
                            physics: const BouncingScrollPhysics(),
                            itemBuilder: (context, index) {
                              var list = data![index];
                              return AnimationConfiguration.staggeredList(
                                position: index,
                                duration: const Duration(milliseconds: 700),
                                child: SlideAnimation(
                                  horizontalOffset: 80.0,
                                  child: FadeInAnimation(
                                    child: GestureDetector(
                                      onTap: () {
                                        var data = {
                                          "id": list.id.toString(),
                                          "order_id": list.orderId.toString(),
                                          "new_order_id":
                                              list.newOrderId.toString()
                                        };

                                        Get.toNamed(MY_ORDERS_DETAIL,
                                            parameters: data);
                                      },
                                      child: pendingDeliveryList(
                                        id: list.id,
                                        orderId: list.id!,
                                        newOrderId: list.newOrderId!,
                                        address: list.deliveryAddress!.address!,
                                        mobileNum: list!.mobileNumber!,
                                        price: list.totalAmount!,
                                        delivery: "Cod",
                                        status: list.status!,
                                        lat: list.deliveryAddress.lattitude,
                                        long: list.deliveryAddress.longitude,
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
        ),
      ],
    );
  }
}
