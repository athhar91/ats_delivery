import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../shared/widgets/general/translate_button.dart';
import '../widgets/theme_switch_button.dart';

class CounterTab extends StatelessWidget {
  const CounterTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('counter_page_title'.tr),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'counter_text'.tr,
              ),
            ],
          ),
        ),
        floatingActionButton: Stack(
          children: const [
            Positioned(
              bottom: 150.0,
              right: 10.0,
              child: ThemeSwitchButton(),
            ),
            Positioned(
              bottom: 80.0,
              right: 10.0,
              child: TranslateButton(),
            ),
          ],
        ) //This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
