import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/resources/app_colors.dart';
import 'package:get/get.dart';
import 'package:flutter_delivery_app/theme/themeService.dart';

import '../controllers/dashboard_controller.dart';

class ThemeSwitchButton extends GetWidget<DashBoardController> {
  const ThemeSwitchButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FloatingActionButton(
        backgroundColor: AppColor.appPrimaryColor,
        heroTag: "themeSwitchButton",
        onPressed: () => ThemeService().switchTheme(),
        tooltip: 'Switch theme',
        child: const Icon(
          Icons.wb_sunny,
        ),
      ),
    );
  }
}
