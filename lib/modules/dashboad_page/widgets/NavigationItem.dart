import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/dashboad_page/widgets/theme_switch_button.dart';
import 'package:flutter_delivery_app/resources/size_config.dart';
import 'package:flutter_delivery_app/router/app_routes.dart';
import 'package:get/get.dart';

import '../controllers/dashboard_controller.dart';

class NavigationItem extends GetView<DashBoardController> {
  @override
  Widget build(BuildContext context) {
    // final Controller = Get.find(DashBoardController());
    return Container(
      child: ListTileTheme(
        textColor: Colors.black.withOpacity(.6),
        iconColor: Colors.black.withOpacity(.6),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipPath(
              child: SizedBox(
                height: 200,
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ClipOval(
                        child: Image.asset(
                          "assets/images/user.png",
                          fit: BoxFit.cover,
                          width: 90.0,
                          height: 90.0,
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.firstName.value,
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                                fontFamily: "PoppinsLight"),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            controller.email.value,
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                                fontFamily: "PoppinsLight"),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),

            // Container(
            //       width: 110.0,
            //       height: 110.0,
            //       margin: const EdgeInsets.only(
            //         top: 20.0,
            //         bottom: 30.0,
            //       ),
            //       clipBehavior: Clip.antiAlias,
            //       decoration: BoxDecoration(
            //         color: Colors.black26,
            //         shape: BoxShape.circle,
            //       ),
            //       child: Image.asset(
            //         'assets/images/flutter_logo.png',
            //         width: 110.0,
            //         height: 110.0,
            //       ),
            //     ),

            ListTile(
              onTap: () {
                Get.toNamed(MY_ORDERS);
              },
              leading: const Icon(Icons.map, color: Colors.white),
              title: const Text('My Orders',
                  style: TextStyle(color: Colors.white)),
            ),
            ListTile(
              onTap: () {
                Get.toNamed(profileRoute);
              },
              leading: const Icon(Icons.person, color: Colors.white),
              title: const Text('My Profile',
                  style: TextStyle(color: Colors.white)),
            ),

            ListTile(
              onTap: () {
                controller.changeLanguageModal();
              },
              leading: const Icon(
                Icons.language,
                color: Colors.white,
              ),
              title: const Text(
                'Switch Language',
                style: TextStyle(color: Colors.white),
              ),
            ),
            //   ExpansionTile(
            //   title: Text(
            //     'Language',
            //     style: TextStyle(color: Colors.white),
            //   ),
            //   leading: const Icon(
            //     Icons.language,
            //     color: Colors.white,
            //   ),
            //   children: [
            //     ListTile(
            //       onTap: () {
            //         Get.toNamed(change_password_route);
            //       },
            //       // leading: const Icon(
            //       //   Icons.language,
            //       //   color: Colors.white,
            //       // ),
            //       title: const Text('English',
            //           style: TextStyle(color: Colors.white)),
            //     ),
            //     ListTile(
            //       onTap: () {
            //         Get.toNamed(change_password_route);
            //       },
            //       // leading: const Icon(
            //       //   Icons.change_circle,
            //       //   color: Colors.white,
            //       // ),
            //       title: const Text('Arabic',
            //           style: TextStyle(color: Colors.white)),
            //     ),
            //   ],
            // ),

            ListTile(
              onTap: () {
                Get.toNamed(change_password_route);
              },
              leading: const Icon(
                Icons.change_circle,
                color: Colors.white,
              ),
              title: const Text('Change Password',
                  style: TextStyle(color: Colors.white)),
            ),
            ListTile(
              onTap: () {
                controller.logoutDialog();
              },
              leading: const Icon(Icons.exit_to_app, color: Colors.white),
              title: const Text('Log out',
                  style: TextStyle(color: Colors.white)),
            ),
            const Spacer(),
            const Align(
              alignment: Alignment.bottomRight,
              child: ThemeSwitchButton(),
            ),
            SizedBox(
              height: SizeConfig.horizontal * 21,
            )
          ],
        ),
      ),
    );
  }
}
