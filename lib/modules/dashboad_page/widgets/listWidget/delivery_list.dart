import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/dashboad_page/controllers/dashboard_controller.dart';
import 'package:get/get.dart';

import '../../../../resources/size_config.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class pendingDeliveryList extends GetView<DashBoardController> {
  String id;
  String orderId;
  String newOrderId;
  String address;
  String mobileNum;
  String price;
  String delivery;
  String status;
  String lat;
  String long;

  pendingDeliveryList(
      {Key? key,
      required this.id,
      required this.orderId,
      required this.newOrderId,
      required this.address,
      required this.mobileNum,
      required this.price,
      required this.delivery,
      required this.status,
      required this.lat,
      required this.long})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: SizeConfig.horizontal * 2.5,
          horizontal: SizeConfig.horizontal * 1),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                offset: const Offset(2.5, 2.5),
                color: Colors.black.withOpacity(0.07),
                blurRadius: 6,
              )
            ]),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.horizontal * 3,
              vertical: SizeConfig.horizontal * 2),
          child: Column(
            children: [
              SizedBox(
                height: SizeConfig.horizontal * 3,
              ),
              Row(
                children: [
                  Text(
                    'Order ID',
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontFamily: 'SegoeReg',
                        fontSize: SizeConfig.horizontal * 4,
                        color: Colors.black87),
                  ),
                  SizedBox(
                    width: SizeConfig.horizontal * 1,
                  ),

                  Text(
                    newOrderId,
                    style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontFamily: 'SegoeReg',
                        fontSize: SizeConfig.horizontal * 4,
                        color: Colors.black87),
                  ),
                  const Spacer(),

                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: (status == "3")
                            ? Colors.orange
                            : (status == "4")
                                ? Colors.blue
                                : Colors.green),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(16, 6, 16, 6),
                      child: Text(
                        (status == "3")
                            ? "Pending"
                            : (status == "4")
                                ? "Out of delivery"
                                : "Delivered",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontFamily: 'SegoeReg',
                            fontSize: SizeConfig.horizontal * 3.5,
                            color: Colors.white),
                      ),
                    ),
                  ),
                  // Text(
                  //   "17-04-2022",
                  //   style: TextStyle(
                  //       fontWeight: FontWeight.w500,
                  //       fontFamily: 'SegoeReg',
                  //       fontSize: SizeConfig.horizontal * 3.5,
                  //       color: Colors.black.withOpacity(0.5)),
                  // ),
                  SizedBox(
                    width: SizeConfig.horizontal * .5,
                  ),
                ],
              ),
              SizedBox(
                height: SizeConfig.horizontal * 0,
              ),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: SizeConfig.horizontal * 3,
                      ),
                      Row(
                        children: [
                          Text(
                            'Address',
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontFamily: 'SegoeReg',
                                fontSize: SizeConfig.horizontal * 3.5,
                                color: Colors.black87),
                          ),
                          SizedBox(
                            width: SizeConfig.horizontal * 4.4,
                          ),
                          Text(
                            ':',
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontFamily: 'SegoeReg',
                                fontSize: SizeConfig.horizontal * 3.5,
                                color: Colors.black.withOpacity(0.5)),
                          ),
                          SizedBox(
                            width: SizeConfig.horizontal * 2,
                          ),
                          SizedBox(
                            width: SizeConfig.horizontal * 44,
                            child: Text(
                              address,
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'SegoeReg',
                                  fontSize: SizeConfig.horizontal * 3,
                                  color: Colors.black.withOpacity(0.5)),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.horizontal * 3,
                      ),
                      Row(
                        children: [
                          Text(
                            'Mobile No',
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontFamily: 'SegoeReg',
                                fontSize: SizeConfig.horizontal * 3.5,
                                color: Colors.black87),
                          ),
                          SizedBox(
                            width: SizeConfig.horizontal * 1.8,
                          ),
                          Text(
                            ':',
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontFamily: 'SegoeReg',
                                fontSize: SizeConfig.horizontal * 3,
                                color: Colors.black.withOpacity(0.5)),
                          ),
                          SizedBox(
                            width: SizeConfig.horizontal * 2,
                          ),
                          Text(
                            mobileNum,
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontFamily: 'SegoeReg',
                                fontSize: SizeConfig.horizontal * 3.5,
                                color: Colors.black.withOpacity(0.5)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.horizontal * 2,
                      ),
                    ],
                  ),
                ],
              ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //   children: [
              //     Container(
              //       child: TextButton(
              //         onPressed: () {
              //           var data = {"id": id, "order_id": orderId};

              //           Get.toNamed(MY_ORDERS_DETAIL, parameters: data);
              //         },
              //         child: Text("Details"),
              //         style: raisedButtonStyle,
              //       ),
              //     ),
              //   ],
              // ),
              SizedBox(
                height: SizeConfig.horizontal * 2,
              ),
              const Divider(
                height: 1,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  IconButton(
                      onPressed: () {
                        controller.makePhoneCall(mobileNum);
                      },
                      icon: FaIcon(
                        FontAwesomeIcons.phone,
                        color: Colors.grey.withOpacity(.7),
                      )),
                  IconButton(
                      onPressed: () {
                        controller.sendWhatsMsg(mobileNum);
                      },
                      icon: FaIcon(
                        FontAwesomeIcons.whatsapp,
                        color: Colors.grey.withOpacity(.7),
                      )),
                  IconButton(
                      onPressed: () {
                        controller.navigateToMap(
                            double.parse(lat), double.parse(long), address);
                      },
                      icon: FaIcon(
                        FontAwesomeIcons.mapLocationDot,
                        color: Colors.grey.withOpacity(.7),
                      ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: Colors.black38,
    primary: Colors.white,
    minimumSize: const Size(50, 28),
    padding: const EdgeInsets.symmetric(horizontal: 25),
    side: const BorderSide(color: Colors.blue, width: 1),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(16)),
    ),
  );
}
