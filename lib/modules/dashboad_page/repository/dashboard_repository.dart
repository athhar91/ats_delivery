import 'package:flutter_delivery_app/data/repository/product_service.dart';

class DashBoardRepository {
  getDashBoardData() {
    return Api().getDashBoardData("12-06-2022", "12-06-2023");
  }

  getPendingOrder(String type) {
    return Api().geOrderList(type);
  }

// edit(obj){
//   return Api().edit( obj );
// }
// add(obj){
//     return Api().add( obj );
// }

}
