import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/profile_page/controllers/signup_controller.dart';
import 'package:flutter_delivery_app/resources/app_colors.dart';
import 'package:get/get.dart';

class NotificanPage extends StatelessWidget {
  const NotificanPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(ProfileController());

    return Scaffold(
        appBar: AppBar(
          title: const Text("Notification"),
          backgroundColor: AppColor.appPrimaryColor,
        ),
        body: const Center(child: Text("Notification not available")));
  }
}
