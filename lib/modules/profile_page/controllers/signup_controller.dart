import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/data/repository/product_service.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

import '../../../data/model/Responces/profile_response.dart';

class ProfileController extends GetxController {
  final fistNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final coutryController = TextEditingController();
  final cityController = TextEditingController();
  final addressController = TextEditingController();
  final emailIdController = TextEditingController();
  final mobileNoController = TextEditingController();

  final data = Data().obs;
  final basicInfo = BasicInfo().obs;

  final formKey = GlobalKey<FormBuilderState>();

  void navigateBack() => Get.back();

  @override
  void onInit() {
    getProfileData();
    // TODO: implement onInit
    super.onInit();
  }

  Future<void> getProfileData() async {
    try {
      EasyLoading.show(status: 'loading...');

      final res = await Api().getProfile();

      if (res.data != null) {
        EasyLoading.dismiss();
        data.value = res.data;
        basicInfo.value = data.value.basicInfo!;

        fistNameController.text = basicInfo.value.firstName!;
        lastNameController.text = basicInfo.value.lastName!;
        coutryController.text = basicInfo.value.countryName!;
        addressController.text = basicInfo.value.address!;
        fistNameController.text = basicInfo.value.firstName!;
        emailIdController.text = basicInfo.value.email!;
        mobileNoController.text = basicInfo.value.mobileNumber!;
      }
    } finally {
      EasyLoading.dismiss();
    }
  }
}
