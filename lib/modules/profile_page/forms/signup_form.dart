import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:flutter_delivery_app/shared/widgets/general/block_button.dart';

import '../controllers/signup_controller.dart';

class SignupForm extends GetWidget<ProfileController> {
  const SignupForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FormBuilder(
        key: controller.formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FormBuilderTextField(
              controller: controller.fistNameController,
              enabled: false,
              name: 'First Name',
              decoration: InputDecoration(
                filled: false,
                isDense: true,
                labelText: 'First Name',
                disabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(context),
                FormBuilderValidators.max(context, 70),
              ]),
            ),
            const SizedBox(
              height: 20,
            ),
            FormBuilderTextField(
              name: 'Last Name ',
              enabled: false,
              controller: controller.lastNameController,
              decoration: InputDecoration(
                filled: false,
                isDense: true,
                labelText: 'Last Name',
                disabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(context),
                FormBuilderValidators.max(context, 70),
              ]),
            ),
            const SizedBox(
              height: 20,
            ),
            FormBuilderTextField(
              enabled: false,
              controller: controller.coutryController,
              name: 'Coutry ',
              decoration: InputDecoration(
                filled: false,
                isDense: true,
                labelText: 'Country',
                disabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(context),
                FormBuilderValidators.max(context, 70),
              ]),
            ),
            const SizedBox(
              height: 20,
            ),
            FormBuilderTextField(
              enabled: false,
              controller: controller.lastNameController,
              name: 'City ',
              decoration: InputDecoration(
                filled: false,
                isDense: true,
                labelText: 'City',
                disabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(context),
                FormBuilderValidators.max(context, 70),
              ]),
            ),
            const SizedBox(
              height: 20,
            ),
            FormBuilderTextField(
              enabled: false,
              controller: controller.addressController,
              name: 'Address ',
              decoration: InputDecoration(
                filled: false,
                isDense: true,
                labelText: 'Address',
                disabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(context),
                FormBuilderValidators.max(context, 70),
              ]),
            ),
            const SizedBox(
              height: 20,
            ),
            FormBuilderTextField(
              enabled: false,
              controller: controller.emailIdController,
              name: 'Email ',
              decoration: InputDecoration(
                filled: false,
                isDense: true,
                labelText: 'Email',
                disabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10.0),
                ),
              ),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(context),
                FormBuilderValidators.max(context, 70),
              ]),
            ),
            const SizedBox(
              height: 20,
            ),
            FormBuilderTextField(
              enabled: false,
              controller: controller.mobileNoController,
              name: 'Mobile Number',
              decoration: InputDecoration(
                filled: false,
                isDense: true,
                disabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                labelText: 'Mobile Number',
              ),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(context),
                FormBuilderValidators.max(context, 70),
              ]),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
                child: Row(children: <Widget>[
              Expanded(
                  child: BlockButton(
                buttonText: "Edit",
                onPressed: () {
                  // controller.formKey.currentState!.save();
                  // if (controller.formKey.currentState!.validate()) {
                  //   print(controller.formKey.currentState!.value);
                  // } else {
                  //   print("validation failed");
                  // }
                },
              ))
            ])),
          ],
        ),
      ),
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Container(
  //     child: FormBuilder(
  //       key: controller.formKey,
  //       child: Column(
  //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //         children: [
  //           FormBuilderTextField(
  //             name: 'fullName',
  //             decoration: const InputDecoration(
  //               prefixIcon: Icon(
  //                 EvaIcons.personOutline,
  //                 size: 18,
  //               ),
  //               filled: false,
  //               isDense: true,
  //               labelText: 'Full name',
  //             ),
  //             validator: FormBuilderValidators.compose([
  //               FormBuilderValidators.required(context),
  //               FormBuilderValidators.max(context, 70),
  //             ]),
  //           ),
  //           const SizedBox(
  //             height: 20,
  //           ),
  //           FormBuilderTextField(
  //             name: 'username',
  //             decoration: const InputDecoration(
  //               prefixIcon: const Icon(
  //                 EvaIcons.personOutline,
  //                 size: 18,
  //               ),
  //               filled: false,
  //               isDense: true,
  //               labelText: 'Username',
  //             ),
  //             validator: FormBuilderValidators.compose([
  //               FormBuilderValidators.required(context),
  //               FormBuilderValidators.max(context, 70),
  //             ]),
  //           ),
  //           const SizedBox(
  //             height: 20,
  //           ),
  //           FormBuilderTextField(
  //             obscureText: true,
  //             name: 'password',
  //             decoration: const InputDecoration(
  //               prefixIcon: Icon(
  //                 EvaIcons.lockOutline,
  //                 size: 18,
  //               ),
  //               filled: false,
  //               isDense: true,
  //               labelText: 'Password',
  //             ),
  //             validator: FormBuilderValidators.compose([
  //               FormBuilderValidators.required(context),
  //               FormBuilderValidators.max(context, 70),
  //             ]),
  //           ),
  //           const SizedBox(
  //             height: 20,
  //           ),
  //           Container(
  //             child: Row(
  //               mainAxisAlignment: MainAxisAlignment.center,
  //               children: [
  //                 InkWell(
  //                     onTap: () => controller.navigateBack(),
  //                     child: Text(
  //                       'Get back to login?',
  //                       style: TextStyle(color: Theme.of(context).primaryColor),
  //                     )),
  //               ],
  //             ),
  //           ),
  //           const SizedBox(
  //             height: 20,
  //           ),
  //           Container(
  //               child: Row(children: <Widget>[
  //             Expanded(
  //                 child: BlockButton(
  //               buttonText: "Signup",
  //               onPressed: () {
  //                 controller.formKey.currentState!.save();
  //                 if (controller.formKey.currentState!.validate()) {
  //                   print(controller.formKey.currentState!.value);
  //                 } else {
  //                   print("validation failed");
  //                 }
  //               },
  //             ))
  //           ])),
  //         ],
  //       ),
  //     ),
  //   );
  // }

}
