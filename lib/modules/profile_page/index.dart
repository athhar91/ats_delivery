import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/profile_page/controllers/signup_controller.dart';
import 'package:flutter_delivery_app/modules/profile_page/forms/signup_form.dart';
import 'package:flutter_delivery_app/resources/app_colors.dart';
import 'package:get/get.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(ProfileController());

    return Scaffold(
        appBar: AppBar(
          title: const Text("Personal information"),
          backgroundColor: AppColor.appPrimaryColor,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Expanded(
            //   child: Container(
            //     height: MediaQuery.of(context).size.height / 4,
            //   ),
            // ),
            // Container(
            //     padding: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
            //     child: Column(
            //         crossAxisAlignment: CrossAxisAlignment.start,
            //         children: [
            //           Text(
            //             'Personal Details',
            //             style: Theme.of(context)
            //                 .primaryTextTheme
            //                 .headline5!
            //                 .copyWith(color: Theme.of(context).primaryColor),
            //           ),
            //         ])),
            Container(padding: const EdgeInsets.all(30), child: const SignupForm())
          ],
        ));
  }
}
