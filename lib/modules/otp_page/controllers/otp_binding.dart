import 'package:get/get.dart';
import 'package:flutter_delivery_app/modules/otp_page/controllers/otp_controller.dart';

class OtpBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OtpController>(() => OtpController());
  }
}
