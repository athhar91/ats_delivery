import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TitleRowWidget extends StatelessWidget {
  String? text1, tetxt2;
  TitleRowWidget(this.text1, this.tetxt2);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text.rich(TextSpan(children: [
          TextSpan(
              text: "$text1" ": ",
              style: const TextStyle(
                fontWeight: FontWeight.w600,
                color: Colors.black54,
                fontSize: 12,
              )),
          TextSpan(
              text: tetxt2,
              style: const TextStyle(
                fontWeight: FontWeight.w600,
                color: Colors.black87,
                fontSize: 12,
              ))
        ])),
        SizedBox(
          width: Get.width * .02,
        ),
      ],
    );
  }
}
