import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/my_order_details_page/components/titleRowWidget.dart';
import 'package:get/get.dart';

import '../../../resources/size_config.dart';

class ItemListWidget extends StatelessWidget {
  List? listItem;

  ItemListWidget({this.listItem});
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal * 0),
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: listItem!.length,
        itemBuilder: (context, index) {
          final orderItem = listItem![index];
          return detailsITemWidget(orderItem);
        },
      ),
    ]);
  }

  Padding detailsITemWidget(orderItem) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 5),
      child: GestureDetector(
          onTap: () {
            // Navigator.of(context)
            //     .push(MaterialPageRoute(builder: (context) {
            //   return PendingOrderDetail();
            // }));
            // Get.toNamed(pendingOrderDetail, arguments: [controller.pendingOrder[index].orderobjectId]);
          },
          child: Container(
            height: Get.width * .36,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                      offset: const Offset(2, 3),
                      color: Colors.black.withOpacity(.1),
                      blurRadius: 3,
                      spreadRadius: 1)
                ]),
            child: Row(
              children: [
                ClipRRect(
                  child: Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: CachedNetworkImage(
                      width: Get.width * .29,
                      height: Get.width * .4,
                      fit: BoxFit.contain,
                      errorWidget: (
                        context,
                        url,
                        error,
                      ) =>
                          const Icon(Icons.error),
                      placeholder: (context, url) => const Center(
                        child: SizedBox(
                          width: 40.0,
                          height: 40.0,
                          child: CircularProgressIndicator(),
                        ),
                      ),
                      imageUrl: orderItem.productImage!,
                    ),
                  ),
                ),
                SizedBox(width: Get.width * .04),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: Get.width * .04,
                      ),
                      Text(orderItem.menuName!,
                          style: const TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 12)),
                      SizedBox(
                        height: Get.width * .02,
                      ),
                      Text(orderItem.productCartonTtype!,
                          style: const TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 10)),
                      // TitleRowWidget("Units",
                      //     orderItem.unitValue! + orderItem.fsUnitName!),
                      SizedBox(
                        height: Get.width * .02,
                      ),
                      TitleRowWidget("VAT", orderItem.taxAmount!),
                      SizedBox(
                        height: Get.width * .02,
                      ),
                      TitleRowWidget("Discount", orderItem.couponCodeAmount!),
                      SizedBox(
                        height: Get.width * .02,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TitleRowWidget("Qty", orderItem.quantity!),
                          Padding(
                            padding: EdgeInsets.only(right: Get.width * .05),
                            child: TitleRowWidget("Price",
                                "SAR " + orderItem.totalPriceAfterDiscount!),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: Get.width * .02,
                      ),
                    ],
                  ),
                )
              ],
            ),
          )),
    );
  }
}
