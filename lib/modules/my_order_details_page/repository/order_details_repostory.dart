import 'package:flutter_delivery_app/data/repository/product_service.dart';

class OrderDeatilsRepository {
  getAll(String id, mOrderId) {
    return Api().geOrderDetails(id, mOrderId);
  }

  updateStatus(String id, mOrderId, status, image, comments) {
    return Api().updateStatus(id, mOrderId, status, image, comments);
  }

  upload(String path) {
    return Api().uploadFileAsFormData(path);
  }
}
