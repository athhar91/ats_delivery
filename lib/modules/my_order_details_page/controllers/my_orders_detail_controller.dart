import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_delivery_app/app_helper/dialog_helpers.dart';
import 'package:get/get.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_picker/image_picker.dart';

import '../../../data/model/Responces/order_details_responce.dart';
import '../repository/order_details_repostory.dart';

class MyOrdersDetailsController extends GetxController {
  final orderDeatilsRepository = OrderDeatilsRepository();
  final data = Data().obs;
  final orderData = OrderData().obs;
  final orderStatus = <OrderStatus>[].obs;
  final deliveryAddress = DeliveryAddress().obs;
  final orderItem = <Item>[].obs;
  final orderId = "".obs;
  final mOrderId = "".obs;
  final newOrderId = "".obs;

  final RxString mOrderDate = "".obs;
  final RxString mOrderTime = "".obs;
  final RxString imagePath = "".obs;

  final RxString imageUrl = "".obs;

  final DateTime date = DateTime.now();
  final dateController = TextEditingController();
  final commentsController = TextEditingController();

  final RxString zipCode = "".obs;
  final RxString cityName = "".obs;
  final RxString countryName = "".obs;
  final RxString address = "".obs;
  final RxString mobileNumber = "".obs;
  Path? imageFile;
  // var myOrdersList = <Order>[].obs;
  final imageFileList = <XFile>[].obs;
  dynamic pickImageError;
  final ImagePicker _picker = ImagePicker();
  String? retrieveDataError;
  File? imagePathTest;

  @override
  void onInit() {
    //Get srguments
    orderId.value = Get.parameters['order_id']!;
    mOrderId.value = Get.parameters['id']!;
    newOrderId.value = Get.parameters['new_order_id']!;

    // retrieveLostData();

    if (orderId.value.isNotEmpty && mOrderId.value.isNotEmpty) {
      getorderList(orderId.value, mOrderId.value);
    }
    super.onInit();
  }

  Future<void> getorderList(String _orderId, String _mOrderId) async {
    EasyLoading.show(status: 'loading...');

    try {
      final res = await orderDeatilsRepository.getAll(_orderId, _mOrderId);
      if (res.data != null) {
        EasyLoading.dismiss();
        data.value = res.data;
        orderData.value = data.value.orderData!;
        orderId.value = orderData.value.id!;
        mOrderId.value = orderData.value.orderId!;
        orderStatus.value = data.value.orderStatus!;
        mOrderDate.value = data.value.orderData!.orderDate!;
        mOrderTime.value = data.value.orderData!.orderTime!;

        deliveryAddress.value = orderData.value.deliveryAddress!;
        print('$deliveryAddress.value');
        orderItem.value = orderData.value.items!;

        // print( orderItem.value[0].menuLangData)

        _addressBlock();
      }
    } finally {
      EasyLoading.dismiss();
    }
  }

  void _addressBlock() {
    zipCode.value = deliveryAddress.value.zipcode!;
    cityName.value = deliveryAddress.value.cityName!;
    countryName.value = deliveryAddress.value.countryName!;
    address.value = deliveryAddress.value.location!;
    mobileNumber.value = deliveryAddress.value.mobileNumber!;
  }

  Future<void> updateStatus(String orderId, String mOrderId, String status,
      String image, String comments) async {
    EasyLoading.show(status: 'loading...');
    // print('iddd' '$order_id');
    // print('iddd' '$m_order_id');
    // Get.find<DashBoardController>().callFunctions();
    // getorderList(orderId.value, mOrderId.value);
    try {
      final res = await orderDeatilsRepository.updateStatus(
          mOrderId, orderId, status, image, comments);
      if (res.responseStatus == "success") {
        EasyLoading.dismiss();

        DialogHelper.showAlertDialog(
            "Success", "Order Status is Updated", Get.overlayContext!);

        // data.value = res.data;
        // orderData.value = data.value.orderData!;
        // orderStatus.value = data.value.orderStatus!;
        // deliveryAddress.value = orderData.value.deliveryAddress!;
        // orderItem.value = orderData.value.items!;
      }
    } finally {
      EasyLoading.dismiss();
    }
  }

  Future<void> upload(String path) async {
    EasyLoading.show(status: 'loading...');

    print("assdsd$path");

    // Get.find<DashBoardController>().callFunctions();
    // getorderList(orderId.value, mOrderId.value);
    // Get.offAllNamed(dashboardRoute);
    try {
      final res = await orderDeatilsRepository.upload(path);

      if (res.responseStatus == "success") {
        EasyLoading.dismiss();

        imageUrl.value = res.data.result;
        print("imageimage${imageUrl.value}");

        DialogHelper.showAlertDialog(
            "Success", "Uploaded", Get.overlayContext!);

        // data.value = res.data;
        // orderData.value = data.value.orderData!;
        // orderStatus.value = data.value.orderStatus!;
        // deliveryAddress.value = orderData.value.deliveryAddress!;
        // orderItem.value = orderData.value.items!;
      } else {
        DialogHelper.showAlertDialog(
            "Error", "Upload failed", Get.overlayContext!);
      }
    } finally {
      EasyLoading.dismiss();
    }
  }

  Future<void> retrieveLostData() async {
    final LostDataResponse response = await _picker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      if (response.files == null) {
        _setImageFileListFromFile(response.file);
      } else {
        imageFileList.value = response.files!;
      }
    } else {
      retrieveDataError = response.exception!.code;
    }
  }

  void _setImageFileListFromFile(XFile? value) {
    imageFileList.value = (value == null ? null : <XFile>[value])!;
  }

  Future<void> onImageButtonPressed(ImageSource source,
      {bool isMultiImage = false}) async {
    // if (_controller != null) {
    //   await _controller!.setVolume(0.0);
    // }

    try {
      var pickedFile = await _picker.pickImage(
        source: source,
      );

      // imagePath.value = pickedFile!.path;
      imagePathTest = File(pickedFile!.path);
      imagePath.value = imagePathTest!.path;
      print('path');
      print(imagePath.value);
      _setImageFileListFromFile(pickedFile);
    } catch (e) {
      pickImageError = e;
    }
  }

  String get final_address =>
      "P.O Box${zipCode.value}, ${cityName.value}${countryName.value}, ${address.value}, \nMobile No:-${mobileNumber.value}";
}
