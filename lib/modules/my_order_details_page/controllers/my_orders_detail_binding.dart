import 'package:get/get.dart';

import 'my_orders_detail_controller.dart';

class MyOrdersDetailsBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MyOrdersDetailsController>(() => MyOrdersDetailsController());
  }
}
