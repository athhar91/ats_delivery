import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/my_order_details_page/controllers/my_orders_detail_controller.dart';
import 'package:flutter_delivery_app/resources/app_colors.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../../resources/size_config.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../components/item_list_widget.dart';
import 'package:intl/intl.dart';

class MyOrdersDetails extends GetView<MyOrdersDetailsController> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var spaceHeight = SizedBox(
      height: Get.width * .04,
    );
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 247, 247, 247),
      appBar: AppBar(
        title: Text(controller.newOrderId.value),
        backgroundColor: AppColor.appPrimaryColor,
        actions: <Widget>[
          Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: const Icon(Icons.more_vert),
              )),
        ],
      ),
      body: Obx(() {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: Get.width * .05),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: Get.width * .06,
                ),
                controller.data.value.orderData != null
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(children: [
                            Text('Order No.:${controller.newOrderId}',
                                style: const TextStyle(
                                    fontWeight: FontWeight.w700, fontSize: 14))
                          ]),
                          Container(
                            height: Get.width * .08,
                            width: Get.width * .25,
                            decoration: BoxDecoration(
                              color: (controller.orderData.value.status == "3"
                                  ? Colors.orange
                                  : controller.orderData.value.status == "4"
                                      ? Colors.blue
                                      : Colors.green),
                              borderRadius: BorderRadius.circular(16),
                              // border: Border.all(color: Colors.green)
                            ),
                            child: Center(
                                child: FittedBox(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                    controller.orderData.value.status == "3"
                                        ? "Pending"
                                        : controller.orderData.value.status ==
                                                "4"
                                            ? "Out for delivery"
                                            : 'Delivered',
                                    style: const TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14,
                                        color: Colors.white)),
                              ),
                            )),
                          )
                        ],
                      )
                    : Container(),
                SizedBox(
                  height: Get.width * .04,
                ),
                controller.data.value.orderData != null
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                            Container(
                              child: Row(
                                children: [
                                  const Text("Date:",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 14)),
                                  const SizedBox(
                                    width: 2,
                                  ),
                                  Text(
                                      '${controller.mOrderDate.value}'
                                      " "
                                      '${controller.mOrderTime.value}',
                                      style: const TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 14,
                                          color: Colors.grey)),
                                ],
                              ),
                            ),
                            Container(
                              height: Get.width * .08,
                              width: Get.width * .25,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                // border: Border.all(color: Colors.green)
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: (controller
                                                .orderData.value.paymentType! ==
                                            "1")
                                        ? Colors.orange
                                        : Colors.green),
                                child: Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(16, 6, 16, 6),
                                  child: Text(
                                    (controller.orderData.value.paymentType! ==
                                            "1")
                                        ? "Cod"
                                        : "Online",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontFamily: 'SegoeReg',
                                        fontSize: SizeConfig.horizontal * 3.5,
                                        color: Colors.white),
                                  ),
                                ),
                              ),
                            )
                          ])
                    : Container(),
                SizedBox(
                  height: Get.width * .04,
                ),
                const Divider(
                  height: .5,
                  color: Colors.grey,
                ),
                SizedBox(
                  height: Get.width * .04,
                ),
                titlewidget("Item Details"),
                SizedBox(
                  height: Get.width * .04,
                ),
                Text("${controller.orderItem.value.length}Items",
                    style: const TextStyle(
                        fontWeight: FontWeight.w600, fontSize: 12)),
                SizedBox(
                  height: Get.width * .04,
                ),
                ItemListWidget(listItem: controller.orderItem),
                SizedBox(
                  height: Get.width * .08,
                ),
                const Divider(
                  height: .5,
                  color: Colors.grey,
                ),
                SizedBox(
                  height: Get.width * .04,
                ),
                titlewidget("Order Information"),
                orderDetailsBlock(),
                productImag(),
                SizedBox(
                  height: Get.width * .04,
                ),
                _statusUpdate(controller.orderData.value.status, context)
              ],
            ),
          ),
        );
      }),
    );
  }

  Widget _statusUpdate(status, context) {
    if (status == "3") {
      return Center(
        child: TextButton(
          style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              backgroundColor:
                  MaterialStateProperty.all<Color>(Colors.blueAccent)),
          onPressed: () {
            controller.updateStatus(controller.orderId.value,
                controller.mOrderId.value, "4", "", "");
          },
          child: const Center(child: Text('Out for delivery')),
        ),
      );
    } else if (status == "6") {
      return TextFormField(
        readOnly: true,
        controller: controller.dateController,
        decoration: const InputDecoration(
          labelText: 'Date',
        ),
        onTap: () async {
          await showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2015),
            lastDate: DateTime(2025),
          ).then((selectedDate) {
            if (selectedDate != null) {
              controller.dateController.text =
                  DateFormat('yyyy-MM-dd').format(selectedDate);
            }
          });
        },
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter date.';
          }
          return null;
        },
      );
    } else if (status == "4") {
      return uploadBlock(context);
    } else {
      return Container();
    }
  }

  Column uploadBlock(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Card(
              clipBehavior: Clip.antiAlias,
              child: SizedBox(
                width: 100,
                height: 100,
                child: IconButton(
                  icon: Icon(
                    FontAwesomeIcons.camera,
                    size: 50,
                    color: AppColor.appPrimaryColor,
                  ),
                  onPressed: () {
                    controller.onImageButtonPressed(ImageSource.camera);
                  },
                ),
              ),
            ),
            Card(
              clipBehavior: Clip.antiAlias,
              child: SizedBox(
                width: 100,
                height: 100,
                child: IconButton(
                  icon: Icon(
                    FontAwesomeIcons.photoFilm,
                    size: 50,
                    color: AppColor.appPrimaryColor,
                  ),
                  onPressed: () {
                    controller.onImageButtonPressed(ImageSource.gallery);
                  },
                ),
              ),
            )
          ],
        ),
        _previewImages(),
        controller.imageFileList.isNotEmpty
            ? Center(
                child: TextButton(
                  style: ButtonStyle(
                      foregroundColor:
                          MaterialStateProperty.all<Color>(Colors.white),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.blueAccent)),
                  onPressed: () {
                    controller.upload(controller.imagePath.value);
                  },
                  child: const Center(child: Text('Upload')),
                ),
              )
            : Container(),
        FormBuilderTextField(
          name: 'Comments',
          controller: controller.commentsController,
          style: const TextStyle(color: Colors.black),
          decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.never,
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
              borderSide: BorderSide(color: Colors.grey, width: 0.0),
            ),
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              ),
            ),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: const BorderSide(color: Colors.white, width: 1.5)),
            filled: true,
            counter: const SizedBox(
              height: 0.0,
            ),
            labelText: 'Comments',
          ),
          validator: FormBuilderValidators.compose([
            // FormBuilderValidators.required(context),
          ]),
          // onSaved: (newValue) => controller.mobileNo = newValue,
        ),
        Center(
          child: TextButton(
            style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                backgroundColor:
                    MaterialStateProperty.all<Color>(Colors.blueAccent)),
            onPressed: () {
              controller.updateStatus(
                  controller.orderId.value,
                  controller.mOrderId.value,
                  "5",
                  controller.imageUrl.value,
                  controller.commentsController.text);
            },
            child: const Center(child: Text('Delivered')),
          ),
        )
      ],
    );
  }

  Text? _getRetrieveErrorWidget() {
    if (controller.retrieveDataError != null) {
      final Text result = Text(controller.retrieveDataError!);
      controller.retrieveDataError = null;
      return result;
    }
    return null;
  }

  Widget _previewImages() {
    // final Text? retrieveError = _getRetrieveErrorWidget();
    // if (retrieveError != null) {
    //   return retrieveError;
    // }
    if (controller.imageFileList.isNotEmpty) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
        child: SizedBox(
          height: 300,
          child: Semantics(
            label: 'image_picker_example_picked_images',
            child: ListView.builder(
              key: UniqueKey(),
              itemBuilder: (BuildContext context, int index) {
                // Why network for web?
                // See https://pub.dev/packages/image_picker#getting-ready-for-the-web-platform
                return Semantics(
                  label: 'image_picker_example_picked_image',
                  child: kIsWeb
                      ? Image.network(controller.imageFileList[index].path)
                      : Image.file(
                          File(controller.imageFileList[index].path),
                          fit: BoxFit.contain,
                        ),
                );
              },
              itemCount: controller.imageFileList.length,
            ),
          ),
        ),
      );
    } else if (controller.pickImageError != null) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          'Pick image error: $controller.pickImageErro',
          textAlign: TextAlign.center,
        ),
      );
    } else {
      return const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text(
          "You have not yet picked an image.",
          textAlign: TextAlign.center,
        ),
      );
    }
  }

  Widget _statusBlock() {
    // final Text? retrieveError = _getRetrieveErrorWidget();
    // if (retrieveError != null) {
    //   return retrieveError;
    // }
    if (controller.imageFileList != null) {
      return Container();
    } else {
      return Container();
    }
  }

  Padding productImag() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Get.width * .00),
      child: controller.orderData.value.status == "5"
          ? Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                adressRowTitleWidget("Proof of Delivery"),
                CachedNetworkImage(
                  width: Get.width * .45,
                  height: Get.width * .6,
                  fit: BoxFit.fitHeight,
                  errorWidget: (
                    context,
                    url,
                    error,
                  ) =>
                      const Icon(Icons.error),
                  placeholder: (context, url) => const Center(
                    child: SizedBox(
                      width: 40.0,
                      height: 40.0,
                      child: CircularProgressIndicator(),
                    ),
                  ),
                  imageUrl: controller.orderData.value.idProof!,
                ),
              ],
            )
          : Container(),
    );
  }

  Padding deliveryDetails(String Str1, String str2) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Get.width * .00),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          adressRowTitleWidget(Str1),
          SizedBox(
            width: Get.width * .05,
          ),
          adressRowWidget(str2)
        ],
      ),
    );
  }

  Padding orderDetailsBlock() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Get.width * .0),
      child: controller.data.value.orderData != null
          ? Column(
              children: [
                SizedBox(
                  height: Get.width * .04,
                ),
                deliveryDetails("Shipping Address", controller.address.value),
                SizedBox(
                  height: Get.width * .05,
                ),
                deliveryDetails(
                    "Payment Method",
                    (controller.orderData.value.paymentType! == "1")
                        ? "Cod"
                        : "Online"),
                SizedBox(
                  height: Get.width * .05,
                ),
                deliveryDetails("Total Order Amount",
                    "SAR ${controller.orderData.value.totalAmount!}"),
                SizedBox(
                  height: Get.width * .05,
                ),
                deliveryDetails("Total Amount to be Collected",
                    "SAR ${controller.orderData.value.finalAmount!}"),
                SizedBox(
                  height: Get.width * .05,
                ),
                SizedBox(
                  width: Get.width * .05,
                ),
              ],
            )
          : Container(),
    );
  }

  Padding titlewidget(String txt) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0),
      child: Text(txt,
          style: const TextStyle(
              fontWeight: FontWeight.w700, fontSize: 14, color: Colors.black)),
    );
  }

  Row adressRowTitleWidget(String str1) {
    return Row(
      children: [
        SizedBox(
          width: 150,
          child: Text("$str1:",
              maxLines: 3,
              style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 13,
                  color: Colors.grey)),
        ),
      ],
    );
  }

  Row adressRowWidget(String str1) {
    return Row(
      children: [
        SizedBox(
          width: Get.width * .4,
          child: Text(str1,
              maxLines: 4,
              style: const TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 13,
              )),
        ),
      ],
    );
  }

  Widget _sizedContainer(Widget child) {
    return SizedBox(
      width: 300.0,
      height: 150.0,
      child: Center(child: child),
    );
  }
}
