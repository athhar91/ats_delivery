import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/signin_page/forms/signin_form.dart';
import 'package:flutter_delivery_app/resources/app_colors.dart';
import 'package:get/get.dart';

class SigninPage extends StatelessWidget {
  const SigninPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColor.appPrimaryColor,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height / 4,
              child: Image.asset(
                'assets/images/splash_logo_ats.png',
                width: Get.width * .6,
              ),
            ),
            Container(
                padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Sign in',
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline5!
                            .copyWith(color: AppColor.appWhite),
                      ),
                    ])),
            Container(padding: const EdgeInsets.all(30), child: const SigninForm())
          ],
        ),
      ),
    );
  }
}
