import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/resources/app_colors.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:flutter_delivery_app/modules/signin_page/controllers/signin_controller.dart';
import 'package:flutter_delivery_app/shared/widgets/general/block_button.dart';

class SigninForm extends GetWidget<SigninController> {
  const SigninForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FormBuilder(
        key: controller.formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Wrap(children: [
              Container(
                margin: const EdgeInsets.only(bottom: 10),
                child: Row(
                  children: const [
                    Icon(
                      EvaIcons.phone,
                      size: 18,
                      color: Colors.white,
                    ),
                    Text(
                      ' Mobile Number',
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              FormBuilderTextField(
                name: 'Mobile Number',
                style: const TextStyle(color: Colors.white),
                keyboardType: TextInputType.number,
                maxLength: 12,
                decoration: InputDecoration(
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  enabledBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                    borderSide:
                        BorderSide(color: Colors.grey, width: 0.0),
                  ),
                  border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: const BorderSide(color: Colors.white, width: 1.5)),
                  filled: true,
                  counter: const SizedBox(
                    height: 0.0,
                  ),
                  labelText: 'Mobile Number',
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(context),
                  FormBuilderValidators.numeric(
                    context,
                  ),
                  // FormBuilderValidators.match(
                  //     context, r'(^(?:[+0]12)?[0-12]{11,12}$)',
                  //     errorText: "Please enter valid number")
                ]),
                onSaved: (newValue) => controller.mobileNo = newValue,
              ),
            ]),
            const SizedBox(
              height: 20,
            ),
            Wrap(
              children: [
                Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  child: Row(
                    children: const [
                      Icon(EvaIcons.lockOutline, size: 18, color: Colors.white),
                      Text(
                        'Password',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ),
                FormBuilderTextField(
                  obscureText: true,
                  name: 'password',
                  style: const TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    enabledBorder: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                      borderSide:
                          BorderSide(color: Colors.grey, width: 0.0),
                    ),
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide:
                            const BorderSide(color: Colors.white, width: 1.5)),
                    filled: true,
                    labelText: 'Password',
                    // labelStyle: TextStyle(color: Colors.white.withOpacity(.8)),
                  ),
                  onSaved: (newValue) => controller.password = newValue,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(context),
                    FormBuilderValidators.minLength(context, 6,
                        errorText: 'Number and length should be 6 minimum!'),
                  ]),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                      onTap: () => controller.navigateToResetPassword(),
                      child: Text(
                        'Forgot Password?',
                        style: TextStyle(color: AppColor.appPrimaryColorDark),
                      )),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
                child: Row(children: <Widget>[
              Expanded(
                child: BlockButton(
                  buttonText: "Signin",
                  onPressed: () {
                    controller.formKey.currentState!.save();
                    if (controller.formKey.currentState!.validate()) {
                      controller.login();
                    } else {
                      print("validation failed");
                    }
                  },
                ),
              )
            ])),
          ],
        ),
      ),
    );
  }
}
