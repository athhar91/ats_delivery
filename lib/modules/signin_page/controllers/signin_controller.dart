import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/data/model/basic_responce.dart';
import 'package:flutter_delivery_app/data/repository/product_service.dart';
import 'package:flutter_delivery_app/router/app_routes.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

import '../../../app_helper/dialog_helpers.dart';
import '../../../app_helper/preference_helper.dart';
import '../../../data/model/Responces/users.dart';

class SigninController extends GetxController {
  final formKey = GlobalKey<FormBuilderState>();
  String? mobileNo;
  String? password;

  BasiResponce? _basiResponce;
  PreferenceHelper? preferenceHelper;

  Users? users;

  Future<void> login() async {
    EasyLoading.show(status: 'loading...');

    final res = await Api().login(
      mobileNo!,
      password!,
    );
    print("res$res");
    if (res != null) {
      if (res.responseStatus == "success") {
        EasyLoading.dismiss();

        users = Users.fromJson(res.data);

        PreferenceHelper.getPref();

        PreferenceHelper.saveUserData(users!);

        navigateToTabPage();
      } else {
        EasyLoading.dismiss();

        Navigator.of(Get.overlayContext!);

        if (res.responseStatus ==
            'Your account is not verified please Verify OTP !') {
          //     var setOTPCookies = {
          //         'mobile_number': $scope.mobileno,
          //         'from' : 'login'
          // }
          //     $cookieStore.put('otpverification', setOTPCookies);
          //     alert('Please Varify OTP');
          //     $location.path('/otp');
          //     return false;
        } else if (res.responseStatus == 'Invalid login credentials') {
          if ("sessionStorage.lang_code" == 'en') {
            DialogHelper.showAlertDialog(
                "error", 'Mobile No. is Invalid', Get.overlayContext!);
            // alert('Mobile No. is Invalid');
          } else {
            DialogHelper.showAlertDialog(
                "error", 'رقم الجوال غير صالح', Get.overlayContext!);
          }
        } else if (res.responseStatus == 'Password does not match !') {
          if ("sessionStorage.lang_code" == 'en') {
            DialogHelper.showAlertDialog(
                "error", 'Password is Invalid', Get.overlayContext!);
          } else {
            DialogHelper.showAlertDialog(
                "error", 'كلمة المرور غير صالحة', Get.overlayContext!);
          }
        } else {
          DialogHelper.showAlertDialog(
              "error", 'Invalid Login Credentials', Get.overlayContext!);
        }
      }
    }

    //model.show('Alert', res!.responseMessage);
  }


  void navigateToSignUp() {
    Get.toNamed('/signup');
  }

  void navigateToResetPassword() {
    Get.toNamed('/reset_password');
  }

  void navigateToTabPage() => Get.offNamed(dashboardRoute);
}
