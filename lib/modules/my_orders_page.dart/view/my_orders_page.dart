import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/dashboad_page/widgets/listWidget/delivery_list.dart';
import 'package:flutter_delivery_app/modules/my_orders_page.dart/controllers/my_orders_controller.dart';
import 'package:flutter_delivery_app/resources/app_colors.dart';
import 'package:flutter_delivery_app/router/app_routes.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';

import '../../../resources/size_config.dart';

class MyOrders extends GetView<MyOrdersController> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Obx(
      () => Scaffold(
          backgroundColor: Colors.grey.withOpacity(.1),
          appBar: AppBar(
            title: const Text(" Orders"),
            backgroundColor: AppColor.appPrimaryColor,
            actions: <Widget>[
              Padding(
                  padding: const EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {},
                    child: const Icon(Icons.more_vert),
                  )),
            ],
          ),
          body: NestedScrollView(
            headerSliverBuilder: (context, innerBoxIsScrolled) => [
              SliverOverlapAbsorber(
                sliver: SliverSafeArea(
                    top: false,
                    sliver: SliverAppBar(
                      floating: true,
                      pinned: false,
                      snap: true,
                      forceElevated: true,
                      elevation: 1,
                      backgroundColor: Colors.white,
                      expandedHeight: 50.0,
                      collapsedHeight: 58.0,
                      flexibleSpace: Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Container(
                          child: TextField(
                            controller: controller.controller.value,
                            onChanged: (value) =>
                                {controller.searchFilter(value)},
                            //  _runFilter(value),
                            decoration: InputDecoration(
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.never,
                              enabledBorder: const OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                                borderSide:
                                    BorderSide(color: Colors.grey, width: 1.0),
                              ),
                              border: const OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(10.0),
                                  ),
                                  borderSide: const BorderSide(
                                      color: Colors.blue, width: 1.5)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: const BorderSide(
                                      color: Colors.red, width: 1.5)),
                              filled: true,
                              counter: const SizedBox(
                                height: 0.0,
                              ),
                              labelText: 'Search Order',
                              suffixIcon: IconButton(
                                onPressed: (() => Get.toNamed(qr_scanner)),
                                icon: Icon(Icons.qr_code),
                              ),
                            ),
                          ),
                        ),
                      ),
                    )),
                handle:
                    NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              )
            ],
            body: AnimationLimiter(
              child: Column(
                children: [
                  Expanded(
                      child: ListView.builder(
                    padding: EdgeInsets.symmetric(
                        horizontal: SizeConfig.horizontal * 2.5),
                    shrinkWrap: true,
                    itemCount: controller.myOrderList.value.length,
                    itemBuilder: (context, index) {
                      var list = controller.myOrderList.value[index];
                      return AnimationConfiguration.staggeredList(
                          position: index,
                          duration: const Duration(milliseconds: 1200),
                          child: SlideAnimation(
                              verticalOffset: 120.0,
                              child: FadeInAnimation(
                                  child: GestureDetector(
                                onTap: () {
                                  print(list.newOrderId);

                                  var data = {
                                    "id": list.id.toString(),
                                    "order_id": list.orderId.toString(),
                                    "new_order_id": list.newOrderId.toString()
                                  };
                                },
                                child: pendingDeliveryList(
                                  id: list.id!,
                                  orderId: list.orderId!,
                                  newOrderId: list.newOrderId!,
                                  address: list.deliveryAddress!.address!,
                                  mobileNum: list.mobileNumber!,
                                  price: list.totalAmount!,
                                  delivery: "Cod",
                                  status: list.status!,
                                  lat: list.deliveryAddress!.lattitude!,
                                  long: list.deliveryAddress!.longitude!,
                                ),
                              ))));
                    },
                  ))
                ],
              ),
            ),
          )),
    );
  }
}
