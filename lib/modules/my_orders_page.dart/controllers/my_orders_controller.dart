import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import '../../../data/model/Responces/myOrders.dart';
import '../../../data/repository/product_service.dart';

class MyOrdersController extends GetxController {
  final data = Data().obs;
  var orders = <Order>[].obs;
  var myOrdersListAll = <Order>[].obs;
  final myOrderList = <Order>[].obs;
  final controller = TextEditingController().obs;
  @override
  void onInit() {
    getorderList("0");

    super.onInit();
  }

  Future<void> getorderList(String type) async {
    EasyLoading.show(status: 'loading...');

    try {
      final res = await Api().geOrderList(type);
      if (res.data != null) {
        EasyLoading.dismiss();
        data.value = res.data!;
        if (type == '3') {
          orders.value = data.value.orders!;
        } else {
          myOrdersListAll.value = data.value.orders!;
          myOrderList.value = myOrdersListAll.value;
        }
      }
    } finally {}
  }

  @override
  void dispose() {
    controller.value.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  void searchFilter(String keyword) {
    var result = <Order>[].obs;
    if (keyword.isEmpty) {
      result.value = myOrdersListAll.value;
    } else {
      result.value = myOrdersListAll.value
          .where((orders) =>
              orders.newOrderId!.toLowerCase().contains(keyword.toLowerCase()))
          .toList();
    }

    myOrderList.value = result.value;
  }
}
