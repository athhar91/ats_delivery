import 'package:flutter_delivery_app/modules/my_orders_page.dart/controllers/qr_controller.dart';
import 'package:get/get.dart';

import 'my_orders_controller.dart';

class QrBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<QrController>(() => QrController());
  }
}
