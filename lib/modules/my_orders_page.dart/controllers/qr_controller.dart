import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import '../../../data/model/Responces/myOrders.dart';
import '../../../data/repository/product_service.dart';

class QrController extends GetxController {
  Barcode? result;
  QRViewController? qrController;
  RxString text = ''.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void dispose() {
    qrController!.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  void onQRViewCreated(QRViewController controller) {
    qrController = controller;

    controller.scannedDataStream.listen((scanData) {
      result = scanData;
    });
  }

  void onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    log('${DateTime.now().toIso8601String()}_onPermissionSet $p');
    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('no Permission')),
      );
    }
  }
}
