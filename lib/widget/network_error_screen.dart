import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

class NetworkErrorItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: Get.height, //Get.height = MediaQuery.of(context).size.height
        width: Get.width, //Get.width = MediaQuery.of(context).size.width
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            //Here I am using an svg icon
            // SvgPicture.asset(
            //   'assets/icons/network.svg',
            //   width: 200,
            //   height: 200,
            //   color: Colors.red[200],
            // ),
            FaIcon(
              FontAwesomeIcons.wifi,
              size: 120,
              color: Colors.blueAccent,
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Internet connection lost!',
              style: TextStyle(fontSize: 16, color: Colors.grey),
            ),
            Text(
              'Check your connection and try again.',
              style: TextStyle(fontSize: 16, color: Colors.grey),
            )
          ],
        ),
      ),
    );
  }
}
