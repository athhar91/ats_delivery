import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:flag/flag.dart';

import '../../../modules/splash_page/app_controllers/app_controller.dart';

class LanguageOptionsWidget extends GetWidget<AppController> {
  const LanguageOptionsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      decoration: BoxDecoration(
        color: Theme.of(context).canvasColor,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      child: Wrap(
        children: [
          ListTile(
            leading: Container(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: const Flag('US',
                      height: 36, width: 36, fit: BoxFit.fill)),
            ),
            title: Text('english'.tr),
            onTap: () => controller.updateLocale(const Locale('en', 'US')),
            trailing: controller.box.read('locale') == "en_US"
                ? const FaIcon(FontAwesomeIcons.globe)
                : const Text(''),
          ),
          ListTile(
            leading: Container(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: const Flag('SAU', height: 36, width: 36, fit: BoxFit.fill)),
            ),
            title: Text('arabic'.tr),
            onTap: () => controller.updateLocale(const Locale('ar', 'DZ')),
            trailing: controller.box.read('locale') == "ar_DZ"
                ? const FaIcon(FontAwesomeIcons.globe)
                : const Text(''),
          )
        ],
      ),
    );
  }
}
