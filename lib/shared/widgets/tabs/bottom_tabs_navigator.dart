import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../modules/splash_page/app_controllers/app_controller.dart';
import '../../../resources/app_colors.dart';

class BottomTabsNavigator extends GetWidget<AppController> {
  const BottomTabsNavigator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => BottomNavyBar(
          selectedIndex: controller.tabIndex.value,
          showElevation: true, // use this to remove appBar's elevation
          onItemSelected: (index) => controller.setTabIndex(index),
          items: [
            // BottomNavyBarItem(
            //     icon: Icon(Icons.add),
            //     title: Text('Counter'),
            //     activeColor: Colors.purpleAccent),
            BottomNavyBarItem(
              icon: const Icon(Icons.apps),
              title: const Text('Home'),
              activeColor: AppColor.appPrimaryColor,
            ),
            BottomNavyBarItem(
                icon: const Icon(Icons.list_alt),
                title: const Text('My Orders'),
                activeColor: AppColor.appPrimaryColor),
          ],
        ));
  }
}
