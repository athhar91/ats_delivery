import 'package:flutter/material.dart';
import 'package:flutter_delivery_app/modules/my_orders_page.dart/view/my_orders_page.dart';
import 'package:get/get.dart';

import '../../../modules/dashboad_page/tabs/home_tab.dart';
import '../../../modules/splash_page/app_controllers/app_controller.dart';

class TabPageSwitcher extends GetWidget<AppController> {
  final List<Widget> _tabPages = [const HomeTab(), MyOrders()];
  TabPageSwitcher({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => _tabPages[controller.tabIndex.value]);
  }
}
