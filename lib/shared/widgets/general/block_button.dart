import 'package:flutter/material.dart';

import '../../../resources/app_colors.dart';

class BlockButton extends StatelessWidget {
  final String buttonText;
  final IconData? icon;
  final Function onPressed;
  final bool isLoading;
  final bool usePrimaryColor;
  const BlockButton(
      {Key? key,
      required this.buttonText,
      required this.onPressed,
      this.icon,
      this.usePrimaryColor = false,
      this.isLoading = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(children: <Widget>[
      Expanded(
        child: MaterialButton(
          elevation: 10,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          padding: const EdgeInsets.symmetric(vertical: 14),
          color: AppColor.appPrimaryColorDark,
          child: isLoading
              ? SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Theme.of(context).canvasColor)),
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      buttonText,
                      style:
                          TextStyle(color: AppColor.appWhite.withOpacity(.5)),
                    ),
                    if (icon != null)
                      SizedBox(
                        height: 30,
                        child: Icon(icon, color: AppColor.appPrimaryColor),
                      )
                  ],
                ),
          onPressed: () => onPressed(),
        ),
      )
    ]));
  }
}
