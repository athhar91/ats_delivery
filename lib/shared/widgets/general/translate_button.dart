import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../modules/splash_page/app_controllers/app_controller.dart';

class TranslateButton extends GetWidget<AppController> {
  const TranslateButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      heroTag: "translateButton",
      backgroundColor: Colors.red,
      onPressed: () => controller.changeLanguageModal(),
      tooltip: 'Change Language',
      child: const Icon(Icons.translate),
    );
  }
}
