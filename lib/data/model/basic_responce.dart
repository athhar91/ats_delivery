// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

BasiResponce basicResponseFromJson(String str) =>
    BasiResponce.fromJson(json.decode(str));

String basicResponseJson(BasiResponce data) => json.encode(data.toJson());

class BasiResponce {
  BasiResponce({
    this.responseCode,
    this.responseStatus,
    this.responseMessage,
    this.data,
  });

  String? responseCode;
  String? responseStatus;
  dynamic responseMessage;
  dynamic data;

  factory BasiResponce.fromJson(Map<String, dynamic> json) => BasiResponce(
        responseCode: json["responseCode"],
        responseStatus: json["responseStatus"],
        responseMessage: json["responseMessage"],
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "responseStatus": responseStatus,
        "responseMessage": responseMessage,
        "data": data,
      };
}
