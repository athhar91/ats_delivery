// To parse this JSON data, do
//
//     final pushTokenModel = pushTokenModelFromJson(jsonString);

import 'dart:convert';

PushTokenModel pushTokenModelFromJson(String str) =>
    PushTokenModel.fromJson(json.decode(str));

String pushTokenModelToJson(PushTokenModel data) => json.encode(data.toJson());

class PushTokenModel {
  PushTokenModel({
    this.responseCode,
    this.responseStatus,
    this.responseMessage,
    this.data,
  });

  String? responseCode;
  String? responseStatus;
  String? responseMessage;
  Data? data;

  factory PushTokenModel.fromJson(Map<String, dynamic> json) => PushTokenModel(
        responseCode: json["responseCode"],
        responseStatus: json["responseStatus"],
        responseMessage: json["responseMessage"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "responseStatus": responseStatus,
        "responseMessage": responseMessage,
        "data": data!.toJson(),
      };
}

class Data {
  Data({
    this.status,
  });

  String? status;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
      };
}
