// To parse this JSON data, do
//
//     final profileResponse = profileResponseFromJson(jsonString);

import 'dart:convert';

ProfileResponce profileResponseFromJson(String str) =>
    ProfileResponce.fromJson(json.decode(str));

String profileResponseToJson(ProfileResponce data) =>
    json.encode(data.toJson());

class ProfileResponce {
  ProfileResponce({
    this.responseCode,
    this.responseStatus,
    this.data,
  });

  String? responseCode;
  String? responseStatus;
  Data? data;

  factory ProfileResponce.fromJson(Map<String, dynamic> json) =>
      ProfileResponce(
        responseCode: json["responseCode"],
        responseStatus: json["responseStatus"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "responseStatus": responseStatus,
        "data": data!.toJson(),
      };
}

class Data {
  Data({
    this.status,
    this.basicInfo,
    this.addressDetails,
  });

  String? status;
  BasicInfo? basicInfo;
  List<dynamic>? addressDetails;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        status: json["status"],
        basicInfo: BasicInfo.fromJson(json["basic_info"]),
        addressDetails:
            List<dynamic>.from(json["address_details"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "basic_info": basicInfo!.toJson(),
        "address_details": List<dynamic>.from(addressDetails!.map((x) => x)),
      };
}

class BasicInfo {
  BasicInfo({
    this.firstName,
    this.lastName,
    this.cityId,
    this.email,
    this.mobileNumber,
    this.status,
    this.userType,
    this.countryId,
    this.lang,
    this.address,
    this.cityName,
    this.countryName,
  });

  String? firstName;
  String? lastName;
  String? cityId;
  String? email;
  String? mobileNumber;
  String? status;
  String? userType;
  String? countryId;
  String? lang;
  String? address;
  dynamic cityName;
  String? countryName;

  factory BasicInfo.fromJson(Map<String, dynamic> json) => BasicInfo(
        firstName: json["first_name"],
        lastName: json["last_name"],
        cityId: json["city_id"],
        email: json["email"],
        mobileNumber: json["mobile_number"],
        status: json["status"],
        userType: json["user_type"],
        countryId: json["country_id"],
        lang: json["lang"],
        address: json["address"],
        cityName: json["CITY_NAME"],
        countryName: json["COUNTRY_NAME"],
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName,
        "last_name": lastName,
        "city_id": cityId,
        "email": email,
        "mobile_number": mobileNumber,
        "status": status,
        "user_type": userType,
        "country_id": countryId,
        "lang": lang,
        "address": address,
        "CITY_NAME": cityName,
        "COUNTRY_NAME": countryName,
      };
}
