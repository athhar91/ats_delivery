// To parse this JSON data, do
//
//     final uploadResponse = uploadResponseFromJson(jsonString);

import 'dart:convert';

UploadResponse uploadResponseFromJson(String str) =>
    UploadResponse.fromJson(json.decode(str));

String uploadResponseToJson(UploadResponse data) => json.encode(data.toJson());

class UploadResponse {
  UploadResponse({
    this.responseCode,
    this.responseStatus,
    this.data,
  });

  String? responseCode;
  String? responseStatus;
  Data? data;

  factory UploadResponse.fromJson(Map<String, dynamic> json) => UploadResponse(
        responseCode: json["responseCode"],
        responseStatus: json["responseStatus"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "responseStatus": responseStatus,
        "data": data!.toJson(),
      };
}

class Data {
  Data({
    this.result,
    this.status,
    this.msg,
  });

  String? result;
  String? status;
  String? msg;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        result: json["result"],
        status: json["status"],
        msg: json["msg"],
      );

  Map<String, dynamic> toJson() => {
        "result": result,
        "status": status,
        "msg": msg,
      };
}
