// To parse this JSON data, do
//
//     final users = usersFromJson(jsonString);

import 'dart:convert';

MyOrdersResponce myOrdersFromJson(String str) =>
    MyOrdersResponce.fromJson(json.decode(str));

String usersToJson(MyOrdersResponce data) => json.encode(data.toJson());

class MyOrdersResponce {
  MyOrdersResponce({
    this.responseCode,
    this.responseStatus,
    this.responseMessage,
    this.data,
  });

  String? responseCode;
  String? responseStatus;
  String? responseMessage;
  Data? data;

  factory MyOrdersResponce.fromJson(Map<String, dynamic> json) =>
      MyOrdersResponce(
        responseCode: json["responseCode"],
        responseStatus: json["responseStatus"],
        responseMessage: json["responseMessage"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "responseStatus": responseStatus,
        "responseMessage": responseMessage,
        "data": data!.toJson(),
      };
}

class Data {
  Data({
    this.orders,
  });

  List<Order>? orders;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        orders: List<Order>.from(json["orders"].map((x) => Order.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "orders": List<dynamic>.from(orders!.map((x) => x.toJson())),
      };
}

class Order {
  Order({
    this.orderId,
    this.id,
    this.newOrderId,
    this.manufacturerId,
    this.assignRetailer,
    this.totalAmount,
    this.taxAmount,
    this.couponCodeId,
    this.couponCodePercent,
    this.couponCodeAmount,
    this.finalAmount,
    this.adminCommissionRate,
    this.status,
    this.deliveryBoyId,
    this.assignedDeliveryBoyDate,
    this.cancellationPolicy,
    this.deliveryDate,
    this.cancelReason,
    this.cancelBy,
    this.rejectOrderReason,
    this.rejectBy,
    this.totalItems,
    this.createdDate,
    this.updatedDate,
    this.isOrderRejected,
    this.isOrderConfirm,
    this.orderConformDate,
    this.isAmountRelease,
    this.idProof,
    this.itemCancelledAmount,
    this.isAnyItemCancelled,
    this.totalDiscountCart,
    this.offerDiscountPecent,
    this.orderType,
    this.userId,
    this.addressId,
    this.deliveryAddress,
    this.mobileNumber,
  });

  String? orderId;
  String? id;
  String? newOrderId;
  String? manufacturerId;
  String? assignRetailer;
  String? totalAmount;
  String? taxAmount;
  String? couponCodeId;
  String? couponCodePercent;
  String? couponCodeAmount;
  String? finalAmount;
  String? adminCommissionRate;
  String? status;
  String? deliveryBoyId;
  String? assignedDeliveryBoyDate;
  String? cancellationPolicy;
  String? deliveryDate;
  String? cancelReason;
  String? cancelBy;
  String? rejectOrderReason;
  String? rejectBy;
  String? totalItems;
  String? createdDate;
  String? updatedDate;
  String? isOrderRejected;
  String? isOrderConfirm;
  String? orderConformDate;
  String? isAmountRelease;
  String? idProof;
  String? itemCancelledAmount;
  String? isAnyItemCancelled;
  dynamic totalDiscountCart;
  dynamic offerDiscountPecent;
  String? orderType;
  String? userId;
  String? addressId;
  DeliveryAddress? deliveryAddress;
  String? mobileNumber;

  factory Order.fromJson(Map<String, dynamic> json) => Order(
        orderId: json["ORDER_ID"],
        id: json["id"],
        newOrderId: json["new_order_id"],
        manufacturerId: json["manufacturer_id"],
        assignRetailer: json["assign_retailer"],
        totalAmount: json["total_amount"],
        taxAmount: json["tax_amount"],
        couponCodeId: json["coupon_code_id"],
        couponCodePercent: json["coupon_code_percent"],
        couponCodeAmount: json["coupon_code_amount"],
        finalAmount: json["final_amount"],
        adminCommissionRate: json["admin_commission_rate"],
        status: json["status"],
        deliveryBoyId: json["delivery_boy_id"],
        assignedDeliveryBoyDate: json["assigned_delivery_boy_date"],
        cancellationPolicy: json["cancellation_policy"],
        deliveryDate: json["delivery_date"],
        cancelReason: json["cancel_reason"],
        cancelBy: json["cancel_by"],
        rejectOrderReason: json["reject_order_reason"],
        rejectBy: json["reject_by"],
        totalItems: json["total_items"],
        createdDate: json["created_date"],
        updatedDate: json["updated_date"],
        isOrderRejected: json["is_order_rejected"],
        isOrderConfirm: json["is_order_confirm"],
        orderConformDate: json["order_conform_date"],
        isAmountRelease: json["is_amount_release"],
        idProof: json["id_proof"],
        itemCancelledAmount: json["item_cancelled_amount"],
        isAnyItemCancelled: json["is_any_item_cancelled"],
        totalDiscountCart: json["total_discount_cart"],
        offerDiscountPecent: json["offer_discount_pecent"],
        orderType: json["order_type"],
        userId: json["user_id"],
        addressId: json["address_id"],
        deliveryAddress: DeliveryAddress.fromJson(json["delivery_address"]),
        mobileNumber: json["mobile_number"],
      );

  Map<String, dynamic> toJson() => {
        "ORDER_ID": orderId,
        "new_order_id": id,
        "order_id": newOrderId,
        "manufacturer_id": manufacturerId,
        "assign_retailer": assignRetailer,
        "total_amount": totalAmount,
        "tax_amount": taxAmount,
        "coupon_code_id": couponCodeId,
        "coupon_code_percent": couponCodePercent,
        "coupon_code_amount": couponCodeAmount,
        "final_amount": finalAmount,
        "admin_commission_rate": adminCommissionRate,
        "status": status,
        "delivery_boy_id": deliveryBoyId,
        "assigned_delivery_boy_date": assignedDeliveryBoyDate,
        "cancellation_policy": cancellationPolicy,
        "delivery_date": deliveryDate,
        "cancel_reason": cancelReason,
        "cancel_by": cancelBy,
        "reject_order_reason": rejectOrderReason,
        "reject_by": rejectBy,
        "total_items": totalItems,
        "created_date": createdDate,
        "updated_date": updatedDate,
        "is_order_rejected": isOrderRejected,
        "is_order_confirm": isOrderConfirm,
        "order_conform_date": orderConformDate,
        "is_amount_release": isAmountRelease,
        "id_proof": idProof,
        "item_cancelled_amount": itemCancelledAmount,
        "is_any_item_cancelled": isAnyItemCancelled,
        "total_discount_cart": totalDiscountCart,
        "offer_discount_pecent": offerDiscountPecent,
        "order_type": orderType,
        "user_id": userId,
        "address_id": addressId,
        "delivery_address": deliveryAddress,
        "mobile_number": mobileNumber,
      };
}

class DeliveryAddress {
  DeliveryAddress({
    this.id,
    this.userId,
    this.country,
    this.mobileNumber,
    this.state,
    this.city,
    this.address,
    this.zipcode,
    this.landmark,
    this.deliveryInstruction,
    this.location,
    this.lattitude,
    this.lang,
    this.longitude,
    this.createdDate,
    this.updatedDate,
    this.countryName,
    this.cityName,
  });

  String? id;
  String? userId;
  String? country;
  String? mobileNumber;
  String? state;
  String? city;
  String? address;
  String? zipcode;
  String? landmark;
  String? deliveryInstruction;
  String? location;
  String? lattitude;
  String? lang;
  String? longitude;
  String? createdDate;
  String? updatedDate;
  String? countryName;
  String? cityName;

  factory DeliveryAddress.fromJson(Map<String, dynamic> json) =>
      DeliveryAddress(
        id: json["id"],
        userId: json["user_id"],
        country: json["country"],
        mobileNumber: json["mobile_number"],
        state: json["state"],
        city: json["city"],
        address: json["address"],
        zipcode: json["zipcode"],
        landmark: json["landmark"],
        deliveryInstruction: json["delivery_instruction"],
        location: json["location"],
        lattitude: json["lattitude"],
        lang: json["lang"],
        longitude: json["longitude"],
        createdDate: json["created_date"],
        updatedDate: json["updated_date"],
        countryName: json["COUNTRY_NAME"],
        cityName: json["CITY_NAME"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "country": country,
        "mobile_number": mobileNumber,
        "state": state,
        "city": city,
        "address": address,
        "zipcode": zipcode,
        "landmark": landmark,
        "delivery_instruction": deliveryInstruction,
        "location": location,
        "lattitude": lattitude,
        "lang": lang,
        "longitude": longitude,
        "created_date": createdDate,
        "updated_date": updatedDate,
        "COUNTRY_NAME": countryName,
        "CITY_NAME": cityName,
      };
}
