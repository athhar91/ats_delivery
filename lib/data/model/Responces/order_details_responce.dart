// To parse this JSON data, do
//
//     final OrderDetails = OrderDetailsFromJson(jsonString);

import 'dart:convert';

OrderDetails orderDetailsFromJson(String str) =>
    OrderDetails.fromJson(json.decode(str));

String orderDetailsToJson(OrderDetails data) => json.encode(data.toJson());

class OrderDetails {
  OrderDetails({
    this.responseCode,
    this.responseStatus,
    this.data,
  });

  String? responseCode;
  String? responseStatus;
  Data? data;

  factory OrderDetails.fromJson(Map<String, dynamic> json) => OrderDetails(
        responseCode: json["responseCode"],
        responseStatus: json["responseStatus"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "responseStatus": responseStatus,
        "data": data!.toJson(),
      };
}

class Data {
  Data({
    this.status,
    this.orderData,
    this.orderStatus,
  });

  String? status;
  OrderData? orderData;
  List<OrderStatus>? orderStatus;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        status: json["status"],
        orderData: OrderData.fromJson(json["order_data"]),
        orderStatus: List<OrderStatus>.from(
            json["order_status"].map((x) => OrderStatus.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "order_data": orderData!.toJson(),
        "order_status": List<dynamic>.from(orderStatus!.map((x) => x.toJson())),
      };
}

class OrderData {
  OrderData({
    this.id,
    this.orderId,
    this.manufacturerId,
    this.assignRetailer,
    this.totalAmount,
    this.taxAmount,
    this.couponCodeId,
    this.couponCodePercent,
    this.couponCodeAmount,
    this.orderDataFinalAmount,
    this.adminCommissionRate,
    this.status,
    this.deliveryBoyId,
    this.assignedDeliveryBoyDate,
    this.cancellationPolicy,
    this.deliveryDate,
    this.cancelReason,
    this.cancelBy,
    this.rejectOrderReason,
    this.rejectBy,
    this.totalItems,
    this.createdDate,
    this.updatedDate,
    this.isOrderRejected,
    this.isOrderConfirm,
    this.orderConformDate,
    this.isAmountRelease,
    this.idProof,
    this.itemCancelledAmount,
    this.isAnyItemCancelled,
    this.totalDiscountCart,
    this.offerDiscountPecent,
    this.orderType,
    this.orderDate,
    this.orderTime,
    this.isWalletUsed,
    this.walletUsedAmount,
    this.finalAmount,
    this.paymentType,
    this.customerName,
    this.mobileNumber,
    this.addressId,
    this.deliveryAddress,
    this.amountToBeReceived,
    this.orderByLeble,
    this.assignedRetailerName,
    this.deliveryBoyName,
    this.items,
  });

  String? id;
  String? orderId;
  String? manufacturerId;
  String? assignRetailer;
  String? totalAmount;
  String? taxAmount;
  String? couponCodeId;
  String? couponCodePercent;
  String? couponCodeAmount;
  String? orderDataFinalAmount;
  String? adminCommissionRate;
  String? status;
  String? deliveryBoyId;
  String? assignedDeliveryBoyDate;
  String? cancellationPolicy;
  String? deliveryDate;
  String? cancelReason;
  String? cancelBy;
  String? rejectOrderReason;
  String? rejectBy;
  String? totalItems;
  String? createdDate;
  String? updatedDate;
  String? isOrderRejected;
  String? isOrderConfirm;
  String? orderConformDate;
  String? isAmountRelease;
  String? idProof;
  String? itemCancelledAmount;
  String? isAnyItemCancelled;
  dynamic totalDiscountCart;
  dynamic offerDiscountPecent;
  String? orderType;
  String? orderDate;
  String? orderTime;
  String? isWalletUsed;
  String? walletUsedAmount;
  String? finalAmount;
  String? paymentType;
  String? customerName;
  String? mobileNumber;
  String? addressId;
  DeliveryAddress? deliveryAddress;
  dynamic amountToBeReceived;
  String? orderByLeble;
  String? assignedRetailerName;
  String? deliveryBoyName;
  List<Item>? items;

  factory OrderData.fromJson(Map<String, dynamic> json) => OrderData(
        id: json["id"],
        orderId: json["order_id"],
        manufacturerId: json["manufacturer_id"],
        assignRetailer: json["assign_retailer"],
        totalAmount: json["total_amount"],
        taxAmount: json["tax_amount"],
        couponCodeId: json["coupon_code_id"],
        couponCodePercent: json["coupon_code_percent"],
        couponCodeAmount: json["coupon_code_amount"],
        orderDataFinalAmount: json["final_amount"],
        adminCommissionRate: json["admin_commission_rate"],
        status: json["status"],
        deliveryBoyId: json["delivery_boy_id"],
        assignedDeliveryBoyDate: json["assigned_delivery_boy_date"],
        cancellationPolicy: json["cancellation_policy"],
        deliveryDate: json["delivery_date"],
        cancelReason: json["cancel_reason"],
        cancelBy: json["cancel_by"],
        rejectOrderReason: json["reject_order_reason"],
        rejectBy: json["reject_by"],
        totalItems: json["total_items"],
        createdDate: json["created_date"],
        updatedDate: json["updated_date"],
        isOrderRejected: json["is_order_rejected"],
        isOrderConfirm: json["is_order_confirm"],
        orderConformDate: json["order_conform_date"],
        isAmountRelease: json["is_amount_release"],
        idProof: json["id_proof"],
        itemCancelledAmount: json["item_cancelled_amount"],
        isAnyItemCancelled: json["is_any_item_cancelled"],
        totalDiscountCart: json["total_discount_cart"],
        offerDiscountPecent: json["offer_discount_pecent"],
        orderType: json["order_type"],
        orderDate: json["ORDER_DATE"],
        orderTime: json["ORDER_TIME"],
        isWalletUsed: json["IS_WALLET_USED"],
        walletUsedAmount: json["WALLET_USED_AMOUNT"],
        finalAmount: json["FINAL_AMOUNT"],
        paymentType: json["payment_type"],
        customerName: json["CUSTOMER_NAME"],
        mobileNumber: json["mobile_number"],
        addressId: json["address_id"],
        deliveryAddress: DeliveryAddress.fromJson(json["delivery_address"]),
        amountToBeReceived: json["amount_to_be_received"],
        orderByLeble: json["order_by_leble"],
        assignedRetailerName: json["assigned_retailer_NAME"],
        deliveryBoyName: json["DELIVERY_BOY_NAME"],
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "order_id": orderId,
        "manufacturer_id": manufacturerId,
        "assign_retailer": assignRetailer,
        "total_amount": totalAmount,
        "tax_amount": taxAmount,
        "coupon_code_id": couponCodeId,
        "coupon_code_percent": couponCodePercent,
        "coupon_code_amount": couponCodeAmount,
        "final_amount": orderDataFinalAmount,
        "admin_commission_rate": adminCommissionRate,
        "status": status,
        "delivery_boy_id": deliveryBoyId,
        "assigned_delivery_boy_date": assignedDeliveryBoyDate,
        "cancellation_policy": cancellationPolicy,
        "delivery_date": deliveryDate,
        "cancel_reason": cancelReason,
        "cancel_by": cancelBy,
        "reject_order_reason": rejectOrderReason,
        "reject_by": rejectBy,
        "total_items": totalItems,
        "created_date": createdDate,
        "updated_date": updatedDate,
        "is_order_rejected": isOrderRejected,
        "is_order_confirm": isOrderConfirm,
        "order_conform_date": orderConformDate,
        "is_amount_release": isAmountRelease,
        "id_proof": idProof,
        "item_cancelled_amount": itemCancelledAmount,
        "is_any_item_cancelled": isAnyItemCancelled,
        "total_discount_cart": totalDiscountCart,
        "offer_discount_pecent": offerDiscountPecent,
        "order_type": orderType,
        "ORDER_DATE": orderDate,
        "ORDER_TIME": orderTime,
        "IS_WALLET_USED": isWalletUsed,
        "WALLET_USED_AMOUNT": walletUsedAmount,
        "FINAL_AMOUNT": finalAmount,
        "payment_type": paymentType,
        "CUSTOMER_NAME": customerName,
        "mobile_number": mobileNumber,
        "address_id": addressId,
        "delivery_address": deliveryAddress,
        "amount_to_be_received": amountToBeReceived,
        "order_by_leble": orderByLeble,
        "assigned_retailer_NAME": assignedRetailerName,
        "DELIVERY_BOY_NAME": deliveryBoyName,
        "items": List<dynamic>.from(items!.map((x) => x.toJson())),
      };
}

class DeliveryAddress {
  DeliveryAddress({
    this.id,
    this.userId,
    this.firstName,
    this.lastName,
    this.country,
    this.mobileNumber,
    this.state,
    this.city,
    this.locality,
    this.address,
    this.addressTitle,
    this.zipcode,
    this.landmark,
    this.deliveryInstruction,
    this.location,
    this.lattitude,
    this.lang,
    this.longitude,
    this.whcode,
    this.warehouseName,
    this.createdDate,
    this.updatedDate,
    this.cityName,
    this.countryName,
  });

  String? id;
  String? userId;
  dynamic firstName;
  dynamic lastName;
  String? country;
  String? mobileNumber;
  dynamic state;
  dynamic city;
  dynamic locality;
  String? address;
  String? addressTitle;
  dynamic zipcode;
  dynamic landmark;
  String? deliveryInstruction;
  String? location;
  String? lattitude;
  dynamic lang;
  String? longitude;
  String? whcode;
  String? warehouseName;
  dynamic createdDate;
  String? updatedDate;
  dynamic cityName;
  String? countryName;

  factory DeliveryAddress.fromJson(Map<String, dynamic> json) =>
      DeliveryAddress(
        id: json["id"],
        userId: json["user_id"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        country: json["country"],
        mobileNumber:
            json["mobile_number"],
        state: json["state"],
        city: json["city"],
        locality: json["locality"],
        address: json["address"],
        addressTitle:
            json["address_title"],
        zipcode: json["zipcode"],
        landmark: json["landmark"],
        deliveryInstruction: json["delivery_instruction"],
        location: json["location"],
        lattitude: json["lattitude"],
        lang: json["lang"],
        longitude: json["longitude"],
        whcode: json["whcode"],
        warehouseName:
            json["warehouse_name"],
        createdDate: json["created_date"],
        updatedDate: json["updated_date"],
        cityName: json["CITY_NAME"],
        countryName: json["COUNTRY_NAME"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "first_name": firstName,
        "last_name": lastName,
        "country": country,
        "mobile_number": mobileNumber,
        "state": state,
        "city": city,
        "locality": locality,
        "address": address,
        "address_title": addressTitle,
        "zipcode": zipcode,
        "landmark": landmark,
        "delivery_instruction":
            deliveryInstruction,
        "location": location,
        "lattitude": lattitude,
        "lang": lang,
        "longitude": longitude,
        "whcode": whcode,
        "warehouse_name": warehouseName,
        "created_date": createdDate,
        "updated_date": updatedDate,
        "CITY_NAME": cityName,
        "COUNTRY_NAME": countryName,
      };
}

class Item {
  Item({
    this.id,
    this.orderId,
    this.menuVarientId,
    this.manufacturerId,
    this.assignRetailer,
    this.quantity,
    this.unitPrice,
    this.totalPriceAfterDiscount,
    this.totalPrice,
    this.couponCodeId,
    this.couponCodePercent,
    this.couponCodeAmount,
    this.tax,
    this.taxAmount,
    this.finalAmount,
    this.unitId,
    this.cStatus,
    this.cancelledReason,
    this.cancelledBy,
    this.cancelledDate,
    this.itemFree,
    this.itemDiscountPercent,
    this.retailerPriceWithoutDiscount,
    this.mainProdId,
    this.image,
    this.menuName,
    this.unitValue,
    this.fsUnitName,
    this.menuLangData,
    this.menuId,
    this.productImage,
    this.productCartonTtype,
  });

  String? id;
  String? orderId;
  String? menuVarientId;
  String? manufacturerId;
  String? assignRetailer;
  String? quantity;
  String? unitPrice;
  String? totalPriceAfterDiscount;
  String? totalPrice;
  String? couponCodeId;
  String? couponCodePercent;
  String? couponCodeAmount;
  String? tax;
  String? taxAmount;
  String? finalAmount;
  String? unitId;
  String? cStatus;
  dynamic cancelledReason;
  dynamic cancelledBy;
  String? cancelledDate;
  String? itemFree;
  dynamic itemDiscountPercent;
  dynamic retailerPriceWithoutDiscount;
  dynamic mainProdId;
  String? image;
  String? menuName;
  String? unitValue;
  String? fsUnitName;
  String? menuLangData;
  String? menuId;
  String? productImage;
  String? productCartonTtype;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["id"],
        orderId: json["order_id"],
        menuVarientId: json["menu_varient_id"],
        manufacturerId: json["manufacturer_id"],
        assignRetailer: json["assign_retailer"],
        quantity: json["quantity"],
        unitPrice: json["unit_price"],
        totalPriceAfterDiscount: json["total_price_after_discount"],
        totalPrice: json["total_price"],
        couponCodeId: json["coupon_code_id"],
        couponCodePercent: json["coupon_code_percent"],
        couponCodeAmount: json["coupon_code_amount"],
        tax: json["tax"],
        taxAmount: json["tax_amount"],
        finalAmount: json["final_amount"],
        unitId: json["unit_id"],
        cStatus: json["c_status"],
        cancelledReason: json["cancelled_reason"],
        cancelledBy: json["cancelled_by"],
        cancelledDate: json["cancelled_date"],
        itemFree: json["item_free"],
        itemDiscountPercent: json["item_discount_percent"],
        retailerPriceWithoutDiscount: json["retailer_price_without_discount"],
        mainProdId: json["main_prod_id"],
        image: json["IMAGE"],
        menuName: json["MENU_NAME"],
        unitValue: json["UNIT_VALUE"],
        fsUnitName: json["FS_UNIT_NAME"],
        menuLangData: json["MENU_LANG_DATA"],
        menuId: json["MENU_ID"],
        productImage: json["product_image"],
        productCartonTtype: json["product_carton_type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "order_id": orderId,
        "menu_varient_id": menuVarientId,
        "manufacturer_id": manufacturerId,
        "assign_retailer": assignRetailer,
        "quantity": quantity,
        "unit_price": unitPrice,
        "total_price_after_discount": totalPriceAfterDiscount,
        "total_price": totalPrice,
        "coupon_code_id": couponCodeId,
        "coupon_code_percent": couponCodePercent,
        "coupon_code_amount": couponCodeAmount,
        "tax": tax,
        "tax_amount": taxAmount,
        "final_amount": finalAmount,
        "unit_id": unitId,
        "c_status": cStatus,
        "cancelled_reason": cancelledReason,
        "cancelled_by": cancelledBy,
        "cancelled_date": cancelledDate,
        "item_free": itemFree,
        "item_discount_percent": itemDiscountPercent,
        "retailer_price_without_discount": retailerPriceWithoutDiscount,
        "main_prod_id": mainProdId,
        "IMAGE": image,
        "MENU_NAME": menuName,
        "UNIT_VALUE": unitValue,
        "FS_UNIT_NAME": fsUnitName,
        "MENU_LANG_DATA": menuLangData,
        "MENU_ID": menuId,
        "product_image": productImage,
        "product_carton_type": productCartonTtype,
      };
}

class OrderStatus {
  OrderStatus({
    this.id,
    this.orderType,
    this.lang,
  });

  String? id;
  String? orderType;
  String? lang;

  factory OrderStatus.fromJson(Map<String, dynamic> json) => OrderStatus(
        id: json["id"],
        orderType: json["order_type"],
        lang: json["lang"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "order_type": orderType,
        "lang": lang,
      };
}
