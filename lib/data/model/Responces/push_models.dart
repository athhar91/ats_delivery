import 'dart:collection';

class PushModel {
  String push_type = "";
  String? subtitle = "";
  String? tickerText = "";
  String vibrate = "";
  String? title = "";
  String? message = "";
  String push_order_id = "";
  static PushModel fromMap(Map<String, dynamic> map) {
    var pushModel = PushModel();
    if (map.containsKey("push_type"))
      pushModel.push_type = map["push_type"].toString();
    if (map.containsKey("subtitle")) pushModel.subtitle = map["subtitle"];
    if (map.containsKey("tickerText")) pushModel.tickerText = map["tickerText"];
    if (map.containsKey("vibrate"))
      pushModel.vibrate = map["vibrate"].toString();
    if (map.containsKey("title")) pushModel.title = map["title"];
    if (map.containsKey("message")) pushModel.message = map["message"];
    if (map.containsKey("push_order_id"))
      pushModel.push_order_id = map["push_order_id"].toString();
    return pushModel;
  }
}
