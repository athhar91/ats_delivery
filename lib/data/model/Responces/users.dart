// To parse this JSON data, do
//
//     final Users = UsersFromJson(jsonString);


import 'package:shared_preferences/shared_preferences.dart';

class Users {
  Users({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.countryCode,
    this.mobileNumber,
    this.status,
    this.otp,
    this.isVerify,
    this.isProfileComplete,
    this.isApproved,
    this.userType,
    this.roleId,
    this.profileImage,
    this.brand,
    this.manufacturerCategory,
    this.type,
    this.address,
    this.street,
    this.addressCountry,
    this.city,
    this.latitude,
    this.longitude,
    this.website,
    this.contactPerson,
    this.phone,
    this.referalCode,
    this.totalLoyaltyPoint,
    this.retailerCreditLimit,
    this.retailerUsedCreditLimit,
    this.commissionRate,
    this.countryId,
    this.createdDate,
    this.updatedDate,
    this.lastLogin,
    this.addedBy,
    this.walletTotalAmount,
    this.walletUsedAmount,
    this.lang,
    this.isRetailerApproved,
    this.isRetailerCreditApproved,
    this.retailerOnlinePayment,
  });

  String? id;
  String? firstName;
  String? lastName;
  String? email;
  String? countryCode;
  String? mobileNumber;
  String? status;
  String? otp;
  String? isVerify;
  String? isProfileComplete;
  String? isApproved;
  String? userType;
  String? roleId;
  String? profileImage;
  String? brand;
  String? manufacturerCategory;
  String? type;
  String? address;
  String? street;
  String? addressCountry;
  String? city;
  String? latitude;
  String? longitude;
  String? website;
  String? contactPerson;
  String? phone;
  String? referalCode;
  String? totalLoyaltyPoint;
  String? retailerCreditLimit;
  String? retailerUsedCreditLimit;
  String? commissionRate;
  String? countryId;
  String? createdDate;
  String? updatedDate;
  String? lastLogin;
  String? addedBy;
  String? walletTotalAmount;
  String? walletUsedAmount;
  String? lang;
  String? isRetailerApproved;
  String? isRetailerCreditApproved;
  String? retailerOnlinePayment;

  factory Users.fromJson(Map<String, dynamic> json) => Users(
        id: json["id"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        email: json["email"],
        countryCode: json["country_code"],
        mobileNumber: json["mobile_number"],
        status: json["status"],
        otp: json["otp"],
        isVerify: json["is_verify"],
        isProfileComplete: json["is_profile_complete"],
        isApproved: json["is_approved"],
        userType: json["user_type"],
        roleId: json["role_id"],
        profileImage: json["profile_image"],
        brand: json["brand"],
        manufacturerCategory: json["manufacturer_category"],
        type: json["type"],
        address: json["address"],
        street: json["street"],
        addressCountry: json["address_country"],
        city: json["city"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        website: json["website"],
        contactPerson: json["contact_person"],
        phone: json["phone"],
        referalCode: json["referal_code"],
        totalLoyaltyPoint: json["total_loyalty_point"],
        retailerCreditLimit: json["retailer_credit_limit"],
        retailerUsedCreditLimit: json["retailer_used_credit_limit"],
        commissionRate: json["commission_rate"],
        countryId: json["country_id"],
        createdDate: json["created_date"],
        updatedDate: json["updated_date"],
        lastLogin: json["last_login"],
        addedBy: json["added_by"],
        walletTotalAmount: json["wallet_total_amount"],
        walletUsedAmount: json["wallet_used_amount"],
        lang: json["lang"],
        isRetailerApproved: json["is_retailer_approved"],
        isRetailerCreditApproved: json["is_retailer_credit_approved"],
        retailerOnlinePayment: json["retailer_online_payment"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "first_name": firstName,
        "last_name": lastName,
        "email": email,
        "country_code": countryCode,
        "mobile_number": mobileNumber,
        "status": status,
        "otp": otp,
        "is_verify": isVerify,
        "is_profile_complete": isProfileComplete,
        "is_approved": isApproved,
        "user_type": userType,
        "role_id": roleId,
        "profile_image": profileImage,
        "brand": brand,
        "manufacturer_category": manufacturerCategory,
        "type": type,
        "address": address,
        "street": street,
        "address_country": addressCountry,
        "city": city,
        "latitude": latitude,
        "longitude": longitude,
        "website": website,
        "contact_person": contactPerson,
        "phone": phone,
        "referal_code": referalCode,
        "total_loyalty_point": totalLoyaltyPoint,
        "retailer_credit_limit": retailerCreditLimit,
        "retailer_used_credit_limit": retailerUsedCreditLimit,
        "commission_rate": commissionRate,
        "country_id": countryId,
        "created_date": createdDate!,
        "updated_date": updatedDate!,
        "last_login": lastLogin,
        "added_by": addedBy,
        "wallet_total_amount": walletTotalAmount,
        "wallet_used_amount": walletUsedAmount,
        "lang": lang,
        "is_retailer_approved": isRetailerApproved,
        "is_retailer_credit_approved": isRetailerCreditApproved,
        "retailer_online_payment": retailerOnlinePayment,
      };

  void save(SharedPreferences pref) {
    pref.setString("id", id!);
    pref.setString("first_name", firstName!);
    pref.setString("last_name", lastName!);
    pref.setString("email", email!);
    pref.setString("country_code", countryCode!);
    pref.setString("mobile_number", mobileNumber!);
    pref.setString("status", status!);
    pref.setString("is_verify", isVerify!);
    pref.setString("is_profile_complete", isProfileComplete!);
    pref.setString("is_approved", isApproved!);
    pref.setString("user_type", userType!);
    pref.setString("role_id", roleId!);
    pref.setString("profile_image", profileImage!);
    pref.setString("brand", brand!);
    pref.setString("manufacturer_category", manufacturerCategory!);
    pref.setString("type", type!);
    pref.setString("address", address!);
    pref.setString("street", street!);
    pref.setString("address_country", addressCountry!);
    pref.setString("city", city!);
    pref.setString("latitude", latitude!);
    pref.setString("longitude", longitude!);
    pref.setString("website", website!);
    pref.setString("contact_person", contactPerson!);
    pref.setString("phone", phone!);
    pref.setString("referal_code", referalCode!);
    pref.setString("total_loyalty_point", totalLoyaltyPoint!);
    pref.setString("retailer_credit_limit", retailerCreditLimit!);
    pref.setString("retailer_used_credit_limit", retailerUsedCreditLimit!);
    pref.setString("commission_rate", commissionRate!);
    pref.setString("country_id", countryId!);
    pref.setString("created_date", createdDate.toString());
    pref.setString("updated_date", updatedDate.toString());
    pref.setString("last_login", lastLogin.toString());
    pref.setString("added_by", addedBy!);
    pref.setString("wallet_total_amount", walletTotalAmount.toString());
    pref.setString("lang", lang!);
    pref.setString("is_retailer_approved", isRetailerApproved!);
    pref.setString("is_retailer_credit_approved", isRetailerCreditApproved!);
    pref.setString("retailer_online_payment", retailerOnlinePayment!);
  }

  static Users getUserFromPref(SharedPreferences pref) {
    var user = Users();
    user.id = pref.getString("id");
    user.firstName = pref.getString("first_name");
    user.lastName = pref.getString("last_name");
    user.email = pref.getString("email");
    user.countryCode = pref.getString("country_code");
    user.mobileNumber = pref.getString("mobile_number");
    user.isVerify = pref.getString("is_verify");
    user.isProfileComplete = pref.getString("is_profile_complete");
    user.isApproved = pref.getString("is_approved");
    user.userType = pref.getString("user_type");
    user.roleId = pref.getString("role_id");
    user.profileImage = pref.getString("profile_image");
    user.brand = pref.getString("brand");
    user.manufacturerCategory = pref.getString("manufacturer_category");
    user.type = pref.getString("type");
    user.address = pref.getString("address");
    user.street = pref.getString("street");
    user.addressCountry = pref.getString("address_country");
    user.city = pref.getString("city");
    user.latitude = pref.getString("latitude");
    user.longitude = pref.getString("longitude");
    user.website = pref.getString("website");
    user.contactPerson = pref.getString("contact_person");
    user.phone = pref.getString("phone");
    user.referalCode = pref.getString("referal_code");
    user.totalLoyaltyPoint = pref.getString("total_loyalty_point");
    user.retailerCreditLimit = pref.getString("retailer_credit_limit");
    user.retailerUsedCreditLimit = pref.getString("retailer_used_credit_limit");
    user.commissionRate = pref.getString("commission_rate");
    user.countryId = pref.getString("country_id");
    user.createdDate = pref.getString("created_date");
    user.updatedDate = pref.getString("updated_date");
    user.lastLogin = pref.getString("last_login");
    user.addedBy = pref.getString("added_by");
    user.walletTotalAmount = pref.getString("wallet_total_amount");
    user.walletUsedAmount = pref.getString("wallet_used_amount");
    user.lang = pref.getString("lang");
    user.isRetailerApproved = pref.getString("is_retailer_approved");
    user.isRetailerCreditApproved =
        pref.getString("is_retailer_credit_approved");
    user.retailerOnlinePayment = pref.getString("retailer_online_payment");
    return user;
  }

  static clearUser(SharedPreferences pref) {
    pref.remove("id");
    pref.remove("first_name");
    pref.remove("last_name");
    pref.remove("email");
    pref.remove("country_code");
    pref.remove("mobile_number");
    pref.remove("is_verify");
    pref.remove("is_profile_complete");
    pref.remove("is_approved");
    pref.remove("user_type");
    pref.remove("role_id");
    pref.remove("profile_image");
    pref.remove("brand");
    pref.remove("manufacturer_category");
    pref.remove("type");
    pref.remove("address");
    pref.remove("street");
    pref.remove("address_country");
    pref.remove("city");
    pref.remove("latitude");
    pref.remove("longitude");
    pref.remove("website");
    pref.remove("contact_person");
    pref.remove("phone");
    pref.remove("referal_code");
    pref.remove("total_loyalty_point");
    pref.remove("retailer_credit_limit");
    pref.remove("retailer_used_credit_limit");
    pref.remove("commission_rate");
    pref.remove("country_id");
    pref.remove("created_date");
    pref.remove("updated_date");
    pref.remove("last_login");
    pref.remove("added_by");
    pref.remove("wallet_total_amount");
    pref.remove("wallet_used_amount");
    pref.remove("lang");
    pref.remove("is_retailer_approved");
    pref.remove("is_retailer_credit_approved");
    pref.remove("retailer_online_payment");
  }
}
