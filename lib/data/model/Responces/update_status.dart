// To parse this JSON data, do
//
//     final updateStatusModel = updateStatusModelFromJson(jsonString);

import 'dart:convert';

UpdateStatusModel updateStatusModelFromJson(String str) =>
    UpdateStatusModel.fromJson(json.decode(str));

String updateStatusModelToJson(UpdateStatusModel data) =>
    json.encode(data.toJson());

class UpdateStatusModel {
  UpdateStatusModel({
    this.responseCode,
    this.responseStatus,
    this.responseMessage,
  });

  String? responseCode;
  String? responseStatus;
  String? responseMessage;

  factory UpdateStatusModel.fromJson(Map<String, dynamic> json) =>
      UpdateStatusModel(
        responseCode: json["responseCode"],
        responseStatus: json["responseStatus"],
        responseMessage: json["responseMessage"],
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "responseStatus": responseStatus,
        "responseMessage": responseMessage,
      };
}

class ResponseMessage {
  ResponseMessage({
    this.status,
    this.errorMsg,
  });

  String? status;
  String? errorMsg;

  factory ResponseMessage.fromJson(Map<String, dynamic> json) =>
      ResponseMessage(
        status: json["status"],
        errorMsg: json["error_msg"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error_msg": errorMsg,
      };
}
