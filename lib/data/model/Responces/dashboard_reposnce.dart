// To parse this JSON data, do
//
//     final dashboardReposnce = dashboardReposnceFromJson(jsonString);

import 'dart:convert';

DashboardReposnce dashboardReposnceFromJson(String str) =>
    DashboardReposnce.fromJson(json.decode(str));

String dashboardReposnceToJson(DashboardReposnce data) =>
    json.encode(data.toJson());

class DashboardReposnce {
  DashboardReposnce({
    this.responseCode,
    this.responseStatus,
    this.responseMessage,
    this.data,
  });

  String? responseCode;
  String? responseStatus;
  String? responseMessage;
  DashBoardData? data;

  factory DashboardReposnce.fromJson(Map<String, dynamic> json) =>
      DashboardReposnce(
        responseCode: json["responseCode"],
        responseStatus: json["responseStatus"],
        responseMessage: json["responseMessage"],
        data: DashBoardData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "responseStatus": responseStatus,
        "responseMessage": responseMessage,
        "data": data!.toJson(),
      };
}

class DashBoardData {
  DashBoardData({
    this.pendingCount,
    this.deliveryCount,
    this.countryCurrency,
    this.oldCount,
  });

  int? pendingCount;
  int? deliveryCount;
  dynamic countryCurrency;
  dynamic oldCount;

  factory DashBoardData.fromJson(Map<String, dynamic> json) => DashBoardData(
        pendingCount: json["PENDING_COUNT"],
        deliveryCount: json["DELIVERY_COUNT"],
        countryCurrency: json["country_currency"],
        oldCount: json["old_count"],
      );

  Map<String, dynamic> toJson() => {
        "PENDING_COUNT": pendingCount,
        "DELIVERY_COUNT": deliveryCount,
        "country_currency": countryCurrency,
        "old_count": oldCount,
      };
}

class CountryCurrency {
  CountryCurrency({
    this.id,
    this.countryName,
    this.currency,
    this.symbol,
  });

  String? id;
  String? countryName;
  String? currency;
  String? symbol;

  factory CountryCurrency.fromJson(Map<String, dynamic> json) =>
      CountryCurrency(
        id: json["id"],
        countryName: json["COUNTRY_NAME"],
        currency: json["currency"],
        symbol: json["symbol"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "COUNTRY_NAME": countryName,
        "currency": currency,
        "symbol": symbol,
      };
}
