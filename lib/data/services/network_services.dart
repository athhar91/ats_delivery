import 'package:flutter_delivery_app/router/app_routes.dart';
import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../../widget/network_error_screen.dart';

class NetworkServices extends GetxService {
  NetworkServices() {
    InternetConnectionChecker()
        .onStatusChange
        .listen((InternetConnectionStatus status) {
      switch (status) {
        case InternetConnectionStatus.connected:
          getNetworkStatus(status);
          break;
        case InternetConnectionStatus.disconnected:
          getNetworkStatus(status);
          break;

        default:
      }
    });
  }

  void getNetworkStatus(InternetConnectionStatus status) {
    print("dfdfdsf$status");
    if (status == InternetConnectionStatus.connected) {
      _validateSession(); //after internet connected it will redirect to home page
    } else {
      // print("disconnecred");
      // Get.snackbar("title", "message");

      Get.dialog(NetworkErrorItem(),
          useSafeArea:
              false); // If internet loss then it will show the NetworkErrorItem widget
    }
  }

  void _validateSession() {
    Get.offAllNamed(splashRoute); //Here redirecting to home page
  }
}
