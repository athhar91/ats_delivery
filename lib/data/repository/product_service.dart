import 'dart:developer';
import 'dart:io';

import 'package:awesome_dio_interceptor/awesome_dio_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:flutter_delivery_app/data/model/Responces/dashboard_reposnce.dart';
import 'package:flutter_delivery_app/data/model/Responces/myOrders.dart';
import 'package:flutter_delivery_app/data/model/Responces/order_details_responce.dart';
import 'package:flutter_delivery_app/data/model/Responces/users.dart';
import 'package:network_logger/network_logger.dart';

import '../../app_helper/preference_helper.dart';

import '../http_service.dart';
import '../model/Responces/profile_response.dart';
import '../model/Responces/push_token_model.dart';
import '../model/Responces/update_status.dart';
import '../model/Responces/upload_response.dart';
import '../model/basic_responce.dart';

class Api extends PreferenceHelper {
  @override
  Api? api;
  PreferenceHelper? preferenceHelper;
  Users? user;
  late Dio dio;

  final Dio _client = Dio(
    BaseOptions(
      baseUrl: 'http://192.81.129.110/viewwater/webservices/',
    ),
  )..interceptors.add(AwesomeDioInterceptor());

  Future<Users> userData() async => user = await PreferenceHelper.getUserData();

  Future login(String mobileNo, String password) async {
    var data = FormData.fromMap({
      'login_mobile_number': mobileNo,
      'login_password': password,
      'language_code': 'en'
    });
    // postRequest('/delivery_boy/login', data);
    try {
      Response response = await _client.post('/delivery_boy/login', data: data);

      return basicResponseFromJson(response.data);
    } on DioError catch (e) {
      return e.response!.data;
    }
  }

  Future<DashboardReposnce> getDashBoardData(
      String fromDate, String toDate) async {
    var data = FormData.fromMap({
      'user_id': await PreferenceHelper.getId(),
      'user_type': await PreferenceHelper.getType(),
      'from_date': fromDate,
      'to_date': toDate,
      'old_count': '10'
    });
    try {
      final response =
          await _client.post('/delivery_boy/home_page', data: data);

      if (response.statusCode == 200) {
        return dashboardReposnceFromJson(response.data);
      } else {
        throw response.statusCode!;
      }
    } on DioError catch (e) {
      return e.response!.data;
    }
  }

  Future 1geOrderList(String type) async {
    var data = FormData.fromMap({
      'country_id': await PreferenceHelper.getCountryId(),
      'user_id': await PreferenceHelper.getId(),
      'user_type': await PreferenceHelper.getType(),
      'page': '0',
      'status': type
    });

    try {
      final response =
          await _client.post('/delivery_boy/order_list', data: data);
      if (response.statusCode == 200) {
        return myOrdersFromJson(response.data);
      } else {
        throw response.statusCode!;
      }
    } on DioError catch (e) {
      return e.response!.data;
    }
  }

  Future geOrderDetails(String id, String mOrderId) async {
    var data = FormData.fromMap({
      'user_id': await PreferenceHelper.getId(),
      'order_id': id,
      'm_order_id': mOrderId,
      'country_id': await PreferenceHelper.getCountryId(),
      'language_code': 'en',
    });
    try {
      final response =
          await _client.post('/delivery_boy/order_detail', data: data);

      if (response.statusCode == 200) {
        return orderDetailsFromJson(response.data);
      } else {
        throw response.statusCode!;
      }
    } on DioError catch (e) {
      return e.response!.data;
    }
  }

  Future tokenRegister(String token) async {
    var data = FormData.fromMap({
      "user_id": await PreferenceHelper.getId(),
      "device_token": token,
      "device_type": (Platform.isIOS ? "ios" : "android")
    });

    try {
      final response = await _client.post('/user_device_token', data: data);

      if (response.statusCode == 200) {
        return pushTokenModelFromJson(response.data);
      } else {
        throw response.statusCode!;
      }
    } on DioError catch (e) {
      return e.response!.data;
    }
  }

  Future updateStatus(
      String id, String mOrderId, status, image, comments) async {
    var data = FormData.fromMap({
      'user_id': await PreferenceHelper.getId(),
      'order_id': id,
      'm_order_id': mOrderId,
      'order_status': status,
      'delivery_date': '2022-05-16',
      'comments': comments,
      'language_code': 'en',
      'id_proof': image,
    });

    try {
      final response =
          await _client.post('/delivery_boy/update_order_status', data: data);
      if (response.statusCode == 200) {
        return updateStatusModelFromJson(response.data);
      } else {
        throw response.statusCode!;
      }
    } on DioError catch (e) {
      return e.response!.data;
    }
  }

  Future getProfile() async {
    var data = FormData.fromMap({
      'user_id': await PreferenceHelper.getId(),
      'language_code': 'en',
    });

    try {
      final response =
          await _client.post('/webservices/basic_info', data: data);
      if (response.statusCode == 200) {
        return profileResponseFromJson(response.data);
      } else {
        throw response.statusCode!;
      }
    } on DioError catch (e) {
      return e.response!.data;
    }
  }

  Future submitResetPassword() async {
    var data = FormData.fromMap({
      'user_id': PreferenceHelper.getId(),
      'old_password': '05008642358',
      'new_password': '05008642359',
      'confirm_new_password': '05008642359',
      'language_code': 'en',
    });

    try {
      final response = await _client.post('webservices/basic_info', data: data);

      if (response.statusCode == 200) {
        return profileResponseFromJson(response.data);
      } else {
        throw response.statusCode!;
      }
    } on DioError catch (e) {
      return e.response!.data;
    }
  }

  Future uploadFileAsFormData(String path) async {
    try {
      final dio = Dio();

      dio.options.headers = {'Content-Type': 'multipart/form-data'};

      final file =
          await MultipartFile.fromFile(path, filename: path.split('/').last);

      final formData = FormData.fromMap(
          {'file': file}); // 'file' - this is an api key, can be different

      final response = await _client.post(
        // or dio.post
        'http://192.81.129.110/viewwater/webservices/delivery_boy/upload_id_proof_image',
        data: formData,
      );

      if (response.statusCode == 200) {
        return uploadResponseFromJson(response.data);
      } else {
        throw response.statusCode!;
      }
    } on DioError catch (e) {
      return e.response!.data;
    }
  }
}
